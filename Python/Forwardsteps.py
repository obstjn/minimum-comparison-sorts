import time
import networkx as nx
import LinExtensions as le
import Graphtools as gt
import matplotlib.pyplot as plt

def forwardsteps(poset, c_left, print_progress=True):
    """
    Checks wether a poset can be sorted in the remaining C-c Comparisons by doing forward steps.
    Only can tell for certain, that sorting is not possible.
    To verify if the result is really sortable, we need to employ backward steps.
    :param poset: poset from which we start
    :param c: comparisons made to arrive at poset
    :param C: Maximum number of Comparisons allowed
    :return: the Set of Posets after each comparison and the dictionary
    """
    #c_left = C - c
    S = [[] for i in range(c_left + 1)]  # the set of all posets in a certain step
    #S_dict = []
    n = len(poset.nodes)
    S[0].append(poset)

    start_of_n = time.time()

    for c in range(c_left + 1):
        new_posets = {}  # dictionary to store the current next posets (these are checked for isomorphism)
        limit = 2 ** (c_left - c)  # C-c
        i = 0  # for better analysis during runtime

        for current_poset in S[c - 1]:
            i += 1  #
            ep, table, num_singletons = le.linear_extensions_singleton(current_poset)
            # ep, table, num_singletons = tf.linext_profiling(current_poset)

            if num_singletons > 2:  # in order to ignore the unnecessary singletons (more than 2) we set a bound
                bound = num_singletons - 2  # 2 singletons remain in the table
            else:
                bound = 0

            time_pos_s = time.time()
            singleton_comp = False  # to safe some singleton comparisons

            for j in range(n - bound - 1):
                if num_singletons > 1 and j == n - bound - 2:  # only True if last j
                    singleton_comp = True

                for k in range(j + 1, n - bound):  # checks all combinations of ujuk until 2 singletons
                    if num_singletons > 1 and k == n - bound - 1 and not singleton_comp:  # last k
                        continue  # skip comparisons with the 2nd singleton
                    p_1 = table[j, k]
                    p_2 = table[k, j]

                    if p_1 == 0 or p_2 == 0:  # comparing already related pair
                        continue
                    if p_1 > limit or p_2 > limit:  # not sortable in remaining comparisons
                        continue
                    else:
                        new_poset = current_poset.copy()
                        new_poset2 = current_poset.copy()

                        if p_1 >= p_2:  # add poset with greater number of linear extensions
                            new_poset.add_edge(j, k)
                            new_poset, new_poset_rev = gt.trans_reduction_and_reverse(new_poset, edge=(j, k))
                            # new_poset = trans_reduction(new_poset, edge=(j, k))
                            current_lin = p_1
                            new_poset2.add_edge(k, j)
                            new_poset2, new_poset_rev2 = gt.trans_reduction_and_reverse(new_poset2, edge=(k, j))
                            # new_poset = gt.trans_reduction(new_poset, edge=(k, j))
                            current_lin2 = p_2
                        else:
                            new_poset.add_edge(k, j)
                            new_poset, new_poset_rev = gt.trans_reduction_and_reverse(new_poset, edge=(k, j))
                            # new_poset = gt.trans_reduction(new_poset, edge=(k, j))
                            current_lin = p_2
                            new_poset2.add_edge(j, k)
                            new_poset2, new_poset_rev2 = gt.trans_reduction_and_reverse(new_poset2, edge=(j, k))
                            # new_poset = trans_reduction(new_poset, edge=(j, k))
                            current_lin2 = p_1

                        iso, id_in_new_posets, id = gt.poset_is_iso(new_poset, new_poset_rev, new_posets, current_lin)
                        if iso:
                            continue
                        else:
                            iso, id2_in_new_posets, id2 = gt.poset_is_iso(new_poset2, new_poset_rev2, new_posets,
                                                                        current_lin2)

                        if iso:  # found an isomorphic or reversed graph
                            continue
                        else:
                            if not id_in_new_posets:  # no isomorphic poset found and no poset with id
                                new_posets[id] = [new_poset]
                                S[c].append(new_poset)
                            else:
                                new_posets[id].append(new_poset)  # add poset to those with same id
                                S[c].append(new_poset)

            time_pos_f = time.time()

            if print_progress:
                n_c = "\rn = {}, c = {}/{} ".format(n, c, c_left)
                p_in_s = "#Posets in S[{}]: {}/{}\t".format(c - 1, i, len(S[c - 1]))
                time_passed = "Time passed: {}s".format(int(time.time() - start_of_n))
                #poset_time = "\tPosetOpTime: {}ms".format(int((time_pos_f - time_pos_s) * 1000))
                print(n_c + p_in_s + time_passed, end='')
        #S_dict.append(new_posets)

    #return S, S_dict
    return S

if __name__=="__main__":
    new_graph = nx.DiGraph()
    new_graph.add_nodes_from(list(range(4)))
    #new_graph.add_edges_from([(0, 2), (1, 3)])
    S = forwardsteps(new_graph, 5)
    print()
    for i in range(6):
        print("i: {}\tPosets: {}".format(i, len(S[i])))
        for p in S[i]:
            plt.figure("Poset in S[{}] {}".format(i, id(p)))
            nx.draw(p, with_labels=True)
    plt.show()
