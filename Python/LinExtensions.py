import networkx as nx
import numpy as np
import time
import math
""""""


# gives the index of a (down-)set
def index_of_set(set):
    return sum([2 ** i for i in set])


# gives the (down-)set to an index
def set_of_index(b):
    set = []
    binarystring = "{0:b}".format(b)
    # binarystring = b
    reversebin = binarystring[::-1]
    for i in range(len(binarystring)):
        if reversebin[i] == '1':
            set.append(i)
    return set


# returns the nodes to which the node has an outgoing edge
def get_outgoing_edges(adjacencymatrix, node):
    return [i for i in range(n) if (adjacencymatrix[node][i] == 1)]


# returns the nodes which have outgoing edges to node
def get_ingoing_edges(adjacencymatrix, node):
    return [i for i in range(n) if (adjacencymatrix[:, node][i] == 1)]


# gives the outgoing edges of the graph in adjacency list representation
def to_outgoing_adjacency_list(graph):
    out_adj_list = [[] for i in range(len(graph.nodes))]
    out_edges = graph.out_edges
    for (i, j) in out_edges:
        out_adj_list[i].append(j)
    return out_adj_list


# gives the ingoing edges of the graph in adjacency list representaion
def to_ingoing_adjacency_list(graph):
    in_adj_list = [[] for i in range(len(graph.nodes))]
    in_edges = graph.in_edges
    for (i, j) in in_edges:
        in_adj_list[j].append(i)
    return in_adj_list


def linear_extensions_matrix(adjacency_matrix):
    """Returns the number of linear extensions (ways a given poset can be topologically sorted) e_p and the table t,
    with t[j,k] = P + ujuk i.e. the number of linear extensions if the relation ujuk is added to the poset.
    Calculation done with adjacency matrix.

    :param adjacency_matrix: the poset represented as a adjecency matrix
    :return: e_p, t

    References
    ----------
    ..[1] M. Peczarski. Towards optimal sorting of 16 elements. CoRR, abs/1108.0866, 2011
      https://arxiv.org/pdf/1108.0866.pdf
    """
    print("Berechnung mit Matrix fehlerhaft!!!")
    n = adjacency_matrix.shape[0]
    table = np.zeros((2 ** n, 3), dtype=np.int64)  # d(V), u(V), visit number for every subset of U
    table[0] = [1, 0, 0]
    table[-1] = [0, 1, 0]

    t = np.zeros((n, n), dtype=np.int64)  # result table with linear Extensions of P + ujuk at position t[j,k]

    queue = ['0' * n]  # downsets to check next in BFS, '0'*n is empty set
    visit = 1  # timestamp

    while (queue):
        binarystring = queue.pop(0)
        while (len(binarystring) < n):
            binarystring = '0' + binarystring

        for i in range(n):
            # check if nodes not in set can be added to set
            if binarystring[i] == '0':
                node = n - 1 - i  # n-1-i is the node since the binary string is traversed in reverse order
                not_downset = False
                for u in get_ingoing_edges(adjacency_matrix,
                                           node):  # maybe change to iteration over column and if (entry == 1)
                    if binarystring[n - 1 - u] == '0':
                        # that means with node it's not a downset, since u is a child/predecessor not in the set (==0)
                        not_downset = True
                        break
                if not_downset:
                    continue
                else:
                    idx = list(binarystring)  # change bit i to 1
                    idx[i] = '1'  # maybe change to xor with bitmask
                    idx = "".join(idx)  #

                    if (table[int(idx, 2)][2] < visit):
                        queue.append(idx)
                        table[int(idx, 2)][2] = visit
                    # addition of values
                    table[int(idx, 2)][0] += table[int(binarystring, 2)][0]

    visit = 2
    queue = ['1' * n]  # complete set U
    while (queue):
        binarystring = queue.pop(0)
        for j in range(n):
            # check if node in the set can be removed from set
            if binarystring[j] == '1':
                node = n - 1 - j
                not_downset = False
                for u in get_outgoing_edges(adjacency_matrix,
                                            node):  # maybe change to iteration over row and if (entry == 1)
                    if binarystring[n - 1 - u] == '1':
                        # that means without node it's not a downset, since u is a parent that is in the set (==1)
                        not_downset = True
                        break
                if not_downset:
                    continue
                else:
                    idx = list(binarystring)  # change bit i to 0
                    idx[j] = '0'  # maybe change to xor with bitmask
                    idx = "".join(idx)  #

                    if (table[int(idx, 2)][2] < visit):
                        queue.append(idx)
                        table[int(idx, 2)][2] = visit

                    # addition of the values
                    table[int(idx, 2)][1] += table[int(binarystring, 2)][1]
                    # calculation of t[j,k]
                    d_v = table[int(idx, 2)][0]
                    u_w = table[int(binarystring, 2)][1]
                    bstring = list(binarystring)

                    for k in range(n):
                        # checks if u_k not in W
                        if bstring[k] == '0':  # position k in the string corresponds with node n-1-k
                            t[node, n - 1 - k] += d_v * u_w
    e_p = table[-1][0]
    return e_p, t


def linear_extensions_list(graph):
    """Returns the number of linear extensions (ways a given poset can be topologically sorted) e_p and the table t,
    with t[j,k] = P + ujuk i.e. the number of linear extensions if the relation ujuk is added to the poset.
    Calculation done with adjacency list.

    :param graph: the poset which is transformed to adjacency list
    :return: e_p, t

    References
    ----------
    ..[1] M. Peczarski. Towards optimal sorting of 16 elements. CoRR, abs/1108.0866, 2011
      https://arxiv.org/pdf/1108.0866.pdf
    """
    outlist = to_outgoing_adjacency_list(graph)
    inlist = to_ingoing_adjacency_list(graph)

    n = len(outlist)
    table = np.zeros((2 ** n, 3), dtype=np.int64)  # d(V), u(V), visit number for every subset of U
    table[0] = [1, 0, 0]
    table[-1] = [0, 1, 0]

    t = np.zeros((n, n), dtype=np.int64)  # result table with linear Extensions of P + ujuk at position t[j,k]

    queue = ['0' * n]  # downsets to check next in BFS, '0'*n is empty set
    visit = 1  # timestamp
    # downsets = [0]  # 0 is the empty set

    while (queue):
        binarystring = queue.pop(0)
        for i in range(n):
            # check if nodes not in set can be added to set (starting with highest node number)
            if binarystring[i] == '0':
                node = n - 1 - i  # n-1-i is the node since the binary string is traversed in reverse order
                not_downset = False
                for u in inlist[node]:
                    if binarystring[n - 1 - u] == '0':
                        # that means with node it's not a downset, since u is a child/predecessor not in the set (==0)
                        not_downset = True
                        break
                if not_downset:
                    continue
                else:
                    idx = list(binarystring)  # change bit i to 1
                    idx[i] = '1'  # maybe change to xor with bitmask
                    idxstr = "".join(idx)  #
                    idx = int(idxstr, 2)


                    if (table[idx][2] < visit):
                        queue.append(idxstr)
                        table[idx][2] = visit
                        # downsets.append(int(idx, 2))
                    # addition of values
                    table[idx][0] += table[int(binarystring, 2)][0]

    visit = 2
    queue = ['1' * n]  # complete set U
    while (queue):
        binarystring = queue.pop(0)
        for j in range(n):
            # check if node in the set can be removed from set
            if binarystring[j] == '1':
                node = n - 1 - j
                not_downset = False
                for u in outlist[node]:
                    if binarystring[n - 1 - u] == '1':
                        # that means without node it's not a downset, since u is a parent that is in the set (==1)
                        not_downset = True
                        break
                if not_downset:
                    continue
                else:
                    idx = list(binarystring)  # change bit i to 0
                    idx[j] = '0'  # maybe change to xor with bitmask
                    idxstr = "".join(idx)  #
                    idx = int(idxstr, 2)

                    if (table[idx][2] < visit):
                        queue.append(idxstr)
                        table[idx][2] = visit

                    # addition of the values
                    table[idx][1] += table[int(binarystring, 2)][1]
                    # calculation of t[j,k]
                    d_v = table[idx][0]
                    u_w = table[int(binarystring, 2)][1]
                    bstring = list(binarystring)

                    for k in range(n - 1 - node):  # only fills upper triangle of t
                        # checks if u_k not in W
                        if bstring[k] == '0':  # position k in the string corresponds with node n-1-k
                            t[node, n - 1 - k] += d_v * u_w
    e_p = table[-1][0]
    for i in range(1, n):  # calculate lower triangle of t
        for j in range(i):
            t[i, j] = e_p - t[j, i]
    return e_p, t


def linear_extensions_singleton(graph):
    """calculates the linear extesions of a poset, removes all but one singleton so that the calculation is faster.
    The singletons need to be the last indices.

    :param graph: the poset P given as nx.DiGraph
    :return: e_P = number of linear extensions
             new_t = the table t with t[j,k] = P + ujuk
    """
    reduced_graph = graph.copy()
    adjacency_matrix = nx.to_numpy_array(graph)
    n = adjacency_matrix.shape[0]
    singletons = []  # the singletons need to be at the last nodes, i.e. all nodes before a singleton are not singleton

    for i in range(n):
        if not any(adjacency_matrix[i]) and not any(adjacency_matrix[:, i]):
            singletons.append(i)

    k = len(singletons)  # number of singletons
    if k <= 1:  # no singletons
        e_P, t = linear_extensions_list(reduced_graph)
        return e_P, t, 0

    #singleton_in_array = singletons[0]
    reduced_graph.remove_nodes_from(singletons[1:])  # only one singleton
    e_P, t_reduced = linear_extensions_list(reduced_graph)

    fac = int(math.factorial(n) / math.factorial(n - k + 1))
    t_reduced = t_reduced * fac
    e_P = e_P * fac
    new_t = np.zeros((n, n), dtype=np.int64)
    for i in range(n):
        for j in range(n):
            if i <= n - k and j <= n - k:  # maybe change to if i,j in singletons and flag
                new_t[i, j] = t_reduced[i, j]  # copy entry
            elif i < n - k and j > n - k:
                new_t[i, j] = t_reduced[i, -1]  # last of the row
            elif i > n - k and j < n - k:
                new_t[i, j] = t_reduced[-1, j]  # last of the column
            elif i >= n - k and j >= n - k:
                if i != j:
                    new_t[i, j] = int(e_P / 2)
                else:
                    new_t[i, j] = 0
    return e_P, new_t, k


""""""
if __name__ == "__main__":
    n = 4
    nodes = range(n)

    new_graph = nx.DiGraph()
    new_graph.add_nodes_from(nodes)
    new_graph.add_edges_from([(0, 2), (1, 2), (1, 3)])
    """Poset P16 with 13 nodes"""
    #new_graph.add_edges_from(
     # [(0, 2), (0, 6), (1, 0), (10, 0), (4, 0), (12, 0), (11, 10), (8, 10), (7, 4), (5, 4), (9, 8), (3, 7)])
    """"""
    adjacency_matrix = nx.to_numpy_array(new_graph)
    """
    start = time.time()
    # print(adjacency_matrix)
    
    # print(adjacency_matrix[:, 1]) #column 1
    # print(not any(adjacency_matrix[:, 1]))
    
    finish = time.time()
    duration = finish - start
    
    # for i in downsets:
    #  print(str(set_of_index(i)) + ":\td = " + str(table[i][0]) +"\tu = "+ str(table[i][1]))
    print("e(P) = " + str(table[-1][0]))
    print("execution time: " + str(int(duration * 1000)) + "ms\n")
    
    
    # print(t)
    """

    start_matrix = time.time()
    ep_matrix, t_matrix = linear_extensions_matrix(adjacency_matrix)
    finish_matrix = time.time()
    duration_matrix = finish_matrix - start_matrix
    print("Matrix:\ne(P) = {}\nexecution time: {}ms\n".format(ep_matrix, int(duration_matrix * 1000)))

    start_list = time.time()
    ep_list, t_list = linear_extensions_list(new_graph)
    finish_list = time.time()
    duration_list = finish_list - start_list

    start_sing = time.time()
    ep_singleton, t_singleton, _ = linear_extensions_singleton(new_graph)
    finish_sing = time.time()
    duration_sing = finish_sing - start_sing

    print("List:\ne(P) = {}\nexecution time: {}ms\n".format(ep_list, int(duration_list * 1000)))
    print("Singleton:\ne(P) = {}\nexecution time: {}ms\n".format(ep_singleton, int(duration_sing * 1000)))

    #print(t_list)
    #print(t_singleton)
    print("\nt is the same: " + str(np.array_equal(t_matrix, t_singleton)))
    print(t_singleton)

    # print(np.array_equal(t_matrix, t_list))

