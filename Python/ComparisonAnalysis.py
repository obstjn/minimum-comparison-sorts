import networkx as nx
import numpy as np
import time
import math
import LinExtensions as le
import sys
import cProfile
import test_functions as tf
import matplotlib.pyplot as plt
import Graphtools as gt

# TODO S_dict überprüfen

def sort_n_in_C_comparisons(n, C):
    """
    Checks wether n elements can be sorted in C Comparisons by doing forward steps.
    Only can tell for certain, that sorting is not possible.
    To verify if the result is really sortable, we need to employ backward steps.
    :param n: number of Elements to be sorted
    :param C: Maximum number of Comparisons allowed
    :return: the Set of Posets after each comparison
    """
    if C < 0:  # special case
        return [[]]

    if C == 0 and n > 1:  # only 1 element can be sorted in 0 comparisons
        return [[]]

    S = [[] for i in range(C + 1)]  # the set of all posets in a certain step
    S_dict = []
    nodes = range(n)
    poset_0 = nx.DiGraph()
    poset_0.add_nodes_from(nodes)
    S[0].append(poset_0)

    start_of_n = time.time()
    new_posets_sizes = ""
    for c in range(1, C + 1):
        new_posets = {}  # dictionary to store the current next posets (these are checked for isomorphism)
        limit = 2 ** (C - c)
        i = 0  # for better analysis during runtime

        for current_poset in S[c - 1]:
            i += 1  #
            ep, table, num_singletons = le.linear_extensions_singleton(current_poset)
            # ep, table, num_singletons = tf.linext_profiling(current_poset)

            if num_singletons > 2:  # in order to ignore the unnecessary singletons (more than 2) we set a bound
                bound = num_singletons - 2  # 2 singletons remain in the table
            else:
                bound = 0

            #time_pos_s = time.time()
            singleton_comp = False  # to safe some singleton comparisons

            for j in range(n - bound - 1):
                if num_singletons > 1 and j == n - bound - 2:  # only True if last j
                    singleton_comp = True

                for k in range(j + 1, n - bound):  # checks all combinations of ujuk until 2 singletons
                    if num_singletons > 1 and k == n - bound - 1 and not singleton_comp:  # last k
                        continue  # skip comparisons with the 2nd singleton
                    p_1 = table[j, k]
                    p_2 = table[k, j]

                    if p_1 == 0 or p_2 == 0:  # comparing already related pair
                        continue
                    if p_1 > limit or p_2 > limit:  # not sortable in remaining comparisons
                        continue
                    else:
                        new_poset = current_poset.copy()
                        new_poset2 = current_poset.copy()

                        if p_1 >= p_2:  # add poset with greater number of linear extensions
                            new_poset.add_edge(j, k)
                            new_poset, new_poset_rev = gt.trans_reduction_and_reverse(new_poset, edge=(j, k))
                            # new_poset = trans_reduction(new_poset, edge=(j, k))
                            current_lin = p_1
                            new_poset2.add_edge(k, j)
                            new_poset2, new_poset_rev2 = gt.trans_reduction_and_reverse(new_poset2, edge=(k, j))
                            # new_poset = gt.trans_reduction(new_poset, edge=(k, j))
                            current_lin2 = p_2
                        else:
                            new_poset.add_edge(k, j)
                            new_poset, new_poset_rev = gt.trans_reduction_and_reverse(new_poset, edge=(k, j))
                            # new_poset = gt.trans_reduction(new_poset, edge=(k, j))
                            current_lin = p_2
                            new_poset2.add_edge(j, k)
                            new_poset2, new_poset_rev2 = gt.trans_reduction_and_reverse(new_poset2, edge=(j, k))
                            # new_poset = trans_reduction(new_poset, edge=(j, k))
                            current_lin2 = p_1

                        iso, id_in_new_posets, id = gt.poset_is_iso(new_poset, new_poset_rev, new_posets, current_lin)
                        if iso:
                            continue
                        else:
                            iso, id2_in_new_posets, id2 = gt.poset_is_iso(new_poset2, new_poset_rev2, new_posets,
                                                                        current_lin2)

                        if iso:  # found an isomorphic or reversed graph
                            continue
                        else:
                            if not id_in_new_posets:  # no isomorphic poset found and no poset with id
                                new_posets[id] = [new_poset]
                                S[c].append(new_poset)
                            else:
                                new_posets[id].append(new_poset)  # add poset to those with same id
                                S[c].append(new_poset)

            #time_pos_f = time.time()

            n_c = "\rn = {}, c = {}/{} ".format(n, c, C)
            p_in_s = "#Posets in S[{}]: {}/{}\t".format(c - 1, i, len(S[c - 1]))
            time_passed = "Time passed: {}s".format(int(time.time() - start_of_n))
            #poset_time = "\tPosetOpTime: {}ms".format(int((time_pos_f - time_pos_s) * 1000))
            print(n_c + p_in_s + time_passed, end='')
        S_dict.append(new_posets)

        max_posets = 0
        avg_posets = 0
        for key in new_posets.keys():
            num = len(new_posets[key])
            avg_posets += num
            max_posets = max(max_posets, num)
        if len(new_posets.keys()) != 0:
            avg_posets = avg_posets / len(new_posets.keys())
        #print("\tmaximum: {} \taverage: {}".format(max_posets, avg_posets))  # print max and avg list length for each c
        new_posets_sizes = new_posets_sizes + "\n{}: maximum: {} \taverage: {}".format(c, max_posets, avg_posets)

        print("\tPosets in S[{}]: {}".format(c, len(S[c])), end='')

    print(new_posets_sizes)
    return S


def main_method():
    itl = [math.ceil(math.log(math.factorial(i), 2)) for i in range(23)]
    print("Information Theoretic lower Bound:\n" + str(itl))
    # n = 12  # the number of elements to be sorted
    # C = 29  # number of comparisons to be checked
    print("Execution time to check if n elements can be sorted in C comparisons,")

    for n in range(1, 11):
        C = itl[n]
        print("n = {}, C = {}:".format(n, C), end='')
        start = time.time()
        S = sort_n_in_C_comparisons(n, C)
        fin = time.time()
        duration = fin - start

        if not S[C]:
            # print("S[C] is empty\nSorting {} elements in {} comparisons is not possible!".format(n, C))
            possible = "NOT Possible!"
        else:
            # print("Sorting {} elements in {} comparisons might be possible".format(n, C))
            possible = "Possible!"
        print("\rn = {}, C = {}: {}ms --> {}".format(n, C, int(duration * 1000), possible), end='')

        num_of_sets = 0
        for set_of_S in S:
            num_of_sets += len(set_of_S)
        print("\tPosets in S: {}".format(num_of_sets))
        for i in range(len(S)):
            print("Posets in S[{}]: {}".format(i, len(S[i])))

        """  # display posets
        for c in range(C, -1, -1):
            print("Posets in S[{}]: {}".format(c, len(S[c])))
            for p in S[c]:
                plt.figure("Poset of S[{}] {}".format(c, id(p)))
                plt.axis('off')
                #nx.draw_networkx(p, pos=nx.drawing.kamada_kawai_layout(p))
                nx.draw(p, with_labels=True)
        plt.show()
        """


if __name__ == "__main__":
    cProfile.run("main_method()")
    #main_method()