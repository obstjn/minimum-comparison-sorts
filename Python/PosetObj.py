import networkx as nx
import LinExtensions as le
import Graphtools as gt
import math
import numpy as np
import time
import typing

class PosetObj:

    def __init__(self, poset_graph: nx.Graph=None):
        self.graph = poset_graph
        if poset_graph is not None:
            self.n = len(poset_graph.nodes)
        else:
            self.n = 0
        self.in_list = []
        self.out_list = []
        self.lin_extensions = 0
        self.linext_table = None
        self.id_tag = ""
        self.id_rev_tag = ""
        self.rev_graph = None
        self.num_singletons = 0
        self.singletons = []
        self.upper_sorting_limit = 0


    def copy(self):
        PO_copy = self.__class__(poset_graph=self.get_graph().copy())
        PO_copy.in_list = self.get_in_list().copy()
        PO_copy.out_list = self.get_out_list().copy()
        PO_copy.lin_extensions = self.lin_extensions
        PO_copy.linext_table = np.copy(self.linext_table)
        PO_copy.id_tag = self.id_tag
        PO_copy.num_singletons = self.num_singletons
        PO_copy.singletons = self.singletons.copy()
        PO_copy.upper_sorting_limit = self.upper_sorting_limit
        return PO_copy


    def get_out_list(self):
        if not self.out_list:
            self.out_list = le.to_outgoing_adjacency_list(self.graph)
            return self.out_list
        else:
            return self.out_list


    def get_in_list(self):
        if not self.in_list:
            self.in_list = le.to_ingoing_adjacency_list(self.graph)
            return self.in_list
        else:
            return self.in_list


    def remove_edge(self, u, v):
        self.graph.remove_edge(u, v)
        self.out_list[u].remove(v)
        self.in_list[v].remove(u)
        self.set_linext(0)


    def remove_singletons_of(self, nodes):
        self.graph.remove_nodes_from(nodes)
        #self.out_list = le.to_outgoing_adjacency_list(self.graph)
        #self.in_list = le.to_ingoing_adjacency_list(self.graph)
        self.n = self.n - len(nodes)

    def add_edge(self, u, v):
        self.graph.add_edge(u, v)
        self.out_list[u].append(v)
        self.in_list[v].append(u)


    def set_num_singletons(self, k):
        self.num_singletons = k


    def set_singletons(self, singletons):
        self.singletons = singletons


    def get_graph(self)-> nx.Graph:
        return self.graph


    def set_graph(self, G):
        self.graph = G

    def set_linext(self, e_P):
        self.lin_extensions = e_P


    def get_linext(self):
        if self.lin_extensions == 0:
            self.calculate_lin_extensions()
        return self.lin_extensions


    def set_linext_table(self, t):
        self.linext_table = t


    def get_linext_table(self):
        return self.linext_table


    def reverse_graph(self):
        """
        Reverses a digraph, i.e. all edges will be flipped.
        Iff the original graph contains the edge (1,2), the reversed graph contains edge (2,1).
        :param graph: the graph to be reversed
        :return: the graph with all edges reversed
        """
        graph = self.get_graph()
        rev = nx.DiGraph()
        rev.add_nodes_from(graph.nodes)
        for (i, j) in graph.edges():
            rev.add_edge(j, i)
        self.rev_graph = rev
        return rev


    def set_reverse_graph(self, rev):
        self.rev_graph = rev


    def draw_poset(self):
        gt.draw_graph(self.get_graph())


    def trans_reduction_and_reverse(self, edge):
        """
        calculates the transitive reduction and also gives its reverse.
        The graph without 'edge' has to be already reduced.
        :param G: graph to be reduced
        :param edge: newly added edge
        :return: The reduction 'TR' and its reverse 'rev'
        """
        TR = self
        G = self.get_graph()
        out_node = edge[0]
        in_node = edge[1]
        rev = self.reverse_graph()
        succ_of_in = [y for x, y in nx.dfs_edges(G, in_node)]
        pred_of_out = [y for x, y in nx.dfs_edges(rev, out_node)]
        out_nbrs = G.out_edges(out_node)  # only neighbors and not successors

        for node, nbr in out_nbrs:
            if nbr in succ_of_in:
                TR.remove_edge(out_node, nbr)
                rev.remove_edge(nbr, out_node)

        for pred in pred_of_out:
            if TR.get_graph().has_edge(pred, in_node):
                TR.remove_edge(pred, in_node)
                rev.remove_edge(in_node, pred)
                continue  # since a futher relation to succ means the graph wasn't reduced beforehand.
            for succ in succ_of_in:
                if TR.get_graph().has_edge(pred, succ):
                    print(pred, succ)
                    TR.remove_edge(pred, succ)
                    rev.remove_edge(succ, pred)

        self.set_reverse_graph(rev)
        return TR, rev


    def calculate_lin_extensions(self):
        """calculates the linear extesions of a poset, removes all but one singleton so that the calculation is faster.
           The singletons need to be the last indices.

           :param graph: the poset P given as nx.DiGraph
           :return: e_P = number of linear extensions
                    new_t = the table t with t[j,k] = P + ujuk
           """
        reduced_PosetObj = self.copy()
        in_list = self.get_in_list()
        out_list = self.get_out_list()

        n = self.n
        singletons = []  # the singletons need to be at the last nodes, i.e. all nodes before a singleton are not singleton

        for i in range(n):
            if not in_list[i] and not out_list[i]:
                singletons.append(i)

        self.set_singletons(singletons)
        self.set_num_singletons(len(singletons))
        k = self.num_singletons  # number of singletons

        if k == 0:  # no singletons
            e_P, t = reduced_PosetObj.linear_extensions_list_PObj()
            self.set_linext(e_P)
            self.set_linext_table(t)
            return e_P, t, 0

        # singleton_in_array = singletons[0]
        reduced_PosetObj.remove_singletons_of(singletons[1:])  # only one singleton remains
        e_P, t_reduced = reduced_PosetObj.linear_extensions_list_PObj()

        fac = int(math.factorial(n) / math.factorial(n - k + 1))
        t_reduced = t_reduced * fac
        e_P = e_P * fac
        new_t = np.zeros((n, n), dtype=np.int64)
        for i in range(n):
            for j in range(n):
                if i <= n - k and j <= n - k:  # maybe change to if i,j in singletons and flag
                    new_t[i, j] = t_reduced[i, j]  # copy entry
                elif i < n - k and j > n - k:
                    new_t[i, j] = t_reduced[i, -1]  # last of the row
                elif i > n - k and j < n - k:
                    new_t[i, j] = t_reduced[-1, j]  # last of the column
                elif i >= n - k and j >= n - k:
                    if i != j:
                        new_t[i, j] = int(e_P / 2)
                    else:
                        new_t[i, j] = 0

        self.set_linext(e_P)
        self.set_linext_table(new_t)
        return e_P, new_t, k


    def linear_extensions_list_PObj(self):
        """Returns the number of linear extensions (ways a given poset can be topologically sorted) e_p and the table t,
        with t[j,k] = P + ujuk i.e. the number of linear extensions if the relation ujuk is added to the poset.
        Calculation done with adjacency list.

        :param graph: the poset which is transformed to adjacency list
        :return: e_p, t

        References
        ----------
        ..[1] M. Peczarski. Towards optimal sorting of 16 elements. CoRR, abs/1108.0866, 2011
          https://arxiv.org/pdf/1108.0866.pdf
        """
        outlist = self.get_out_list()
        inlist = self.get_in_list()

        n = self.n
        table = np.zeros((2 ** n, 3), dtype=np.int64)  # d(V), u(V), visit number for every subset of U
        table[0] = [1, 0, 0]
        table[-1] = [0, 1, 0]

        t = np.zeros((n, n), dtype=np.int64)  # result table with linear Extensions of P + ujuk at position t[j,k]

        queue = ['0' * n]  # downsets to check next in BFS, '0'*n is empty set
        visit = 1  # timestamp
        # downsets = [0]  # 0 is the empty set

        while (queue):
            binarystring = queue.pop(0)
            for i in range(n):
                # check if nodes not in set can be added to set (starting with highest node number)
                if binarystring[i] == '0':
                    node = n - 1 - i  # n-1-i is the node since the binary string is traversed in reverse order
                    not_downset = False
                    for u in inlist[node]:
                        if binarystring[n - 1 - u] == '0':
                            # that means with node it's not a downset, since u is a child/predecessor not in the set (==0)
                            not_downset = True
                            break
                    if not_downset:
                        continue
                    else:
                        idx = list(binarystring)  # change bit i to 1
                        idx[i] = '1'  # maybe change to xor with bitmask
                        idxstr = "".join(idx)  #
                        idx = int(idxstr, 2)

                        if (table[idx][2] < visit):
                            queue.append(idxstr)
                            table[idx][2] = visit
                            # downsets.append(int(idx, 2))
                        # addition of values
                        table[idx][0] += table[int(binarystring, 2)][0]

        visit = 2
        queue = ['1' * n]  # complete set U
        while (queue):
            binarystring = queue.pop(0)
            for j in range(n):
                # check if node in the set can be removed from set
                if binarystring[j] == '1':
                    node = n - 1 - j
                    not_downset = False
                    for u in outlist[node]:
                        if binarystring[n - 1 - u] == '1':
                            # that means without node it's not a downset, since u is a parent that is in the set (==1)
                            not_downset = True
                            break
                    if not_downset:
                        continue
                    else:
                        idx = list(binarystring)  # change bit i to 0
                        idx[j] = '0'  # maybe change to xor with bitmask
                        idxstr = "".join(idx)  #
                        idx = int(idxstr, 2)

                        if (table[idx][2] < visit):
                            queue.append(idxstr)
                            table[idx][2] = visit

                        # addition of the values
                        table[idx][1] += table[int(binarystring, 2)][1]
                        # calculation of t[j,k]
                        d_v = table[idx][0]
                        u_w = table[int(binarystring, 2)][1]
                        bstring = list(binarystring)

                        for k in range(n - 1 - node):  # only fills upper triangle of t
                            # checks if u_k not in W
                            if bstring[k] == '0':  # position k in the string corresponds with node n-1-k
                                t[node, n - 1 - k] += d_v * u_w
        e_p = table[-1][0]
        for i in range(1, n):  # calculate lower triangle of t
            for j in range(i):
                t[i, j] = e_p - t[j, i]
        return e_p, t


    def forwardsteps(self, c_left, print_progress=True):
        """
        Checks wether a poset can be sorted in the remaining C-c Comparisons by doing forward steps.
        Only can tell for certain, that sorting is not possible.
        To verify if the result is really sortable, we need to employ backward steps.
        :param poset: poset from which we start
        :param c: comparisons made to arrive at poset
        :param C: Maximum number of Comparisons allowed
        :return: the Set of Posets after each comparison and the dictionary
        """
        #c_left = C - c
        S = [[] for i in range(c_left + 1)]  # the set of all posets in a certain step
        #S_dict = []
        n = self.n
        S[0].append(self)

        start_of_n = time.time()

        for c in range(c_left + 1):
            new_posets = {}  # dictionary to store the current next posets (these are checked for isomorphism)
            limit = 2 ** (c_left - c)  # C-c
            i = 0  # for better analysis during runtime

            for current_poset in S[c - 1]:
                i += 1  #
                ep, table, num_singletons = current_poset.calculate_lin_extensions()
                # ep, table, num_singletons = tf.linext_profiling(current_poset)

                if num_singletons > 2:  # in order to ignore the unnecessary singletons (more than 2) we set a bound
                    bound = num_singletons - 2  # 2 singletons remain in the table
                else:
                    bound = 0

                time_pos_s = time.time()
                singleton_comp = False  # to safe some singleton comparisons

                for j in range(n - bound - 1):
                    if num_singletons > 1 and j == n - bound - 2:  # only True if last j
                        singleton_comp = True

                    for k in range(j + 1, n - bound):  # checks all combinations of ujuk until 2 singletons
                        if num_singletons > 1 and j == n - bound - 1 and not singleton_comp:  # last k
                            continue  # skip comparisons with the 2nd singleton
                        p_1 = table[j, k]
                        p_2 = table[k, j]

                        if p_1 == 0 or p_2 == 0:  # comparing already related pair
                            continue
                        if p_1 > limit or p_2 > limit:  # not sortable in remaining comparisons
                            continue
                        else:
                            new_poset = current_poset.copy()
                            new_poset2 = current_poset.copy()

                            if p_1 >= p_2:  # add poset with greater number of linear extensions
                                new_poset.add_edge(j, k)
                                new_poset, new_poset_rev_graph = new_poset.trans_reduction_and_reverse(edge=(j, k))
                                # new_poset = trans_reduction(new_poset, edge=(j, k))
                                current_lin = p_1
                                new_poset2.add_edge(k, j)
                                new_poset2, new_poset_rev_graph2 = new_poset2.trans_reduction_and_reverse(edge=(k, j))
                                # new_poset = gt.trans_reduction(new_poset, edge=(k, j))
                                current_lin2 = p_2
                            else:
                                new_poset.add_edge(k, j)
                                new_poset, new_poset_rev_graph = new_poset.trans_reduction_and_reverse(edge=(k, j))
                                # new_poset = gt.trans_reduction(new_poset, edge=(k, j))
                                current_lin = p_2
                                new_poset2.add_edge(j, k)
                                new_poset2, new_poset_rev_graph2 = new_poset2.trans_reduction_and_reverse(edge=(j, k))
                                # new_poset = trans_reduction(new_poset, edge=(j, k))
                                current_lin2 = p_1

                            iso, id_in_new_posets, id = gt.poset_is_iso(new_poset, new_poset_rev_graph, new_posets, current_lin)
                            new_poset.id_tag = id
                            if iso:
                                continue
                            else:
                                iso, id2_in_new_posets, id2 = gt.poset_is_iso(new_poset2, new_poset_rev_graph2, new_posets,
                                                                            current_lin2)
                                new_poset2.id_tag = id2

                            if iso:  # found an isomorphic or reversed graph
                                continue
                            else:
                                if not id_in_new_posets:  # no isomorphic poset found and no poset with id
                                    new_posets[id] = [new_poset]
                                    S[c].append(new_poset)
                                else:
                                    new_posets[id].append(new_poset)  # add poset to those with same id
                                    S[c].append(new_poset)

                time_pos_f = time.time()

                if print_progress:
                    n_c = "\rn = {}, c = {}/{} ".format(n, c, c_left)
                    p_in_s = "#Posets in S[{}]: {}/{}\t".format(c - 1, i, len(S[c - 1]))
                    time_passed = "Time passed: {}s".format(int(time.time() - start_of_n))
                    #poset_time = "\tPosetOpTime: {}ms".format(int((time_pos_f - time_pos_s) * 1000))
                    print(n_c + p_in_s + time_passed, end='')
            #S_dict.append(new_posets)

        #return S, S_dict
        return S

if __name__=='__main__':
    new_graph = nx.DiGraph()
    new_graph.add_nodes_from(range(4))
    #new_graph.add_edges_from([(0, 2), (1, 2), (1, 3)])

    po1 = PosetObj(new_graph)
    print(po1.calculate_lin_extensions())
    S = po1.forwardsteps(5, print_progress=True)