import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import LinExtensions as le
import sys


def reverse_graph(graph):
    """
    Reverses a digraph, i.e. all edges will be flipped.
    Iff the original graph contains the edge (1,2), the reversed graph contains edge (2,1).
    :param graph: the graph to be reversed
    :return: the graph with all edges reversed
    """
    rev = nx.DiGraph()
    rev.add_nodes_from(graph.nodes)
    for (i, j) in graph.edges():
        rev.add_edge(j, i)
    return rev


def graphs_differ(G1, G2):
    """
    States wether the graphs differ by comparing the adjcencymatrixes.
    :param G1: graph 1
    :param G2: graph 2
    :return: True if the graphs differ
    """
    a1 = nx.to_numpy_array(G1)
    a2 = nx.to_numpy_array(G2)
    return not np.array_equal(a1, a2)


def trans_reduction_and_reverse(G, edge):
    """
    calculates the transitive reduction and also gives its reverse.
    The graph without 'edge' has to be already reduced.
    :param G: graph to be reduced
    :param edge: newly added edge
    :return: The reduction 'TR' and its reverse 'rev'
    """
    TR = G.copy()
    out_node = edge[0]
    in_node = edge[1]
    rev = reverse_graph(G)
    succ_of_in = [y for x, y in nx.dfs_edges(G, in_node)]
    pred_of_out = [y for x, y in nx.dfs_edges(rev, out_node)]
    out_nbrs = G.out_edges(out_node)  # only neighbors and not successors

    for node, nbr in out_nbrs:
        if nbr in succ_of_in:
            TR.remove_edge(out_node, nbr)
            rev.remove_edge(nbr, out_node)

    for pred in pred_of_out:
        if TR.has_edge(pred, in_node):
            TR.remove_edge(pred, in_node)
            rev.remove_edge(in_node, pred)
            continue  # since a futher relation to succ means the graph wasn't reduced beforehand.
        for succ in succ_of_in:
            if TR.has_edge(pred, succ):
                TR.remove_edge(pred, succ)
                rev.remove_edge(succ, pred)
    return TR, rev


def trans_reduction(G, edge=None):
    """
    Verison1 (edge is None) Copied from networkx libary, but no check for DAG,
    science all graphs are Posets and therefore have no cycles.
    Version2 (else) slightly faster implementation that uses the fact
    that only one edge is added and before it was already reduced.

    :param G: graph to be reduced
    :return: transitive reduction of the graph
    """
    if edge is None:
        TR = nx.DiGraph()
        TR.add_nodes_from(G.nodes())
        descendants = {}
        # count before removing set stored in descendants
        check_count = dict(G.in_degree)
        for u in G:
            u_nbrs = set(G[u])
            for v in G[u]:
                if v in u_nbrs:
                    if v not in descendants:
                        descendants[v] = {y for x, y in nx.dfs_edges(G, v)}
                    u_nbrs -= descendants[v]
                check_count[v] -= 1
                if check_count[v] == 0:
                    del descendants[v]
            TR.add_edges_from((u, v) for v in u_nbrs)

        return TR
    else:
        TR2 = G.copy()
        out_node = edge[0]
        in_node = edge[1]
        rev = reverse_graph(G)
        succ_of_in = [y for x, y in nx.dfs_edges(G, in_node)]
        pred_of_out = [y for x, y in nx.dfs_edges(rev, out_node)]
        out_nbrs = G.out_edges(out_node)
        for node, nbr in out_nbrs:
            if nbr in succ_of_in:
                TR2.remove_edge(out_node, nbr)

        for pred in pred_of_out:
            if TR2.has_edge(pred, in_node):
                TR2.remove_edge(pred, in_node)
            for succ in succ_of_in:
                if TR2.has_edge(pred, succ):
                    TR2.remove_edge(pred, succ)
        return TR2


def intersection(lst1, lst2):
    """
    Gives as list of elements that are in list1 and in list2
    :param lst1: list1
    :param lst2: list2
    :return: list of intersection
    """
    return [value for value in lst1 if value in lst2]


def color_refinement(G):
    """
    Compute the 1-dim Weisfeiler Lehman aka color refinement of the given Graph G.
    Each Node will get a 'color' based on the color of its neighbors, until a stable
    coloring is reached. This algorithm calculates the first stable coloring.
    :param G: the Graph to color
    :return: the first stable coloring of each node

    References
    ----------
    ..[1] M. Grohe, K.Kersting, M. Mladenov, P. Schweitzer. Color Refinement and its Applications, 2017
      https://www.lics.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaabbtcqu
    """
    n = len(G.nodes)
    out_nbrs = le.to_outgoing_adjacency_list(G)
    in_nbrs = le.to_ingoing_adjacency_list(G)
    #out_nbrs = [G[n] for n in G]

    C = np.ones(n, dtype=int) # initial Coloring
    P = {}  # P[x] = verticies having color x
    P[1] = list(range(n))
    cmin = 1
    cmax = 1
    D_out = np.zeros(n, dtype=int)  # stores number of neighbors of the current color
    D_in = np.zeros(n, dtype=int)  # stores number of neighbors of the current color
    Q = [1]
    while Q:
        q = Q.pop(0)
        for node in range(n):
            D_out[node] = len(intersection(out_nbrs[node], P[q]))
            D_in[node] = len(intersection(in_nbrs[node], P[q]))

        B = {}  # Partition of colors
        for node in range(n):
            if (C[node], D_out[node], D_in[node]) in B:
                B[(C[node], D_out[node], D_in[node])].append(node)
            else:
                B[(C[node], D_out[node], D_in[node])] = [node]

        B_sorted_keys = sorted(B.keys())  # = [(C[node], D_out[node], D_in[node])]
        B_part = []  # Partition of the vertices ordered by (C[node], D_out[node], D_in[node])
        for key in B_sorted_keys:
            B_part.append(B[key])

        for c in range(cmin, cmax + 1):
            k1 = 0
            k2 = 0
            k1_found = False
            for k in range(len(B_sorted_keys)):  # find lowest index k1 and highest index k2 where C(v) == c
                if B_sorted_keys[k][0] == c and not k1_found:
                    k1 = k
                    k1_found = True

                if k1_found and B_sorted_keys[k][0] != c:
                    break  # last time c is found
                else:
                    k2 = k

            i_star = 0  # argmax i of len(B_part[i])
            max_partition = 0
            for i in range(k1, k2 + 1):
                if len(B_part[i]) > max_partition:
                    i_star = i
                    max_partition = len(B_part[i])

            for i in range(k1, k2 + 1):
                if i == i_star:
                    continue
                else:
                    Q.append(cmax + i + 1)

        cmin = cmax + 1
        cmax = cmax + len(B_part)

        for b in range(cmin, cmax + 1):
            P[b] = B_part[b - cmin]
            for v in P[b]:
                C[v] = b
    return C

gleichid = 0
def poset_is_iso(new_poset, new_poset_rev, new_posets, current_lin):
    """
    Checks if a given poset (or its reverse) is isomorphic to a list (in a dict) of already added posets.

    :param new_poset: The poset to check
    :param new_poset_rev: the reverse of the poset
    :param new_posets: The dictionary that has the posets to check
    :param current_lin: the number of linear Extensions of new_poset
    :return: True and id if one is isomorphic, else False and wether or not the poset has just been added to new_posets
     and the id of the poset to check
    """
    list_of_out_deg_as_str = list(map(str, sorted(d for n, d in new_poset.out_degree())))
    list_of_in_deg_as_str = list(map(str, sorted(d for n, d in new_poset.in_degree())))
    id = ''.join(list_of_out_deg_as_str) + ' ' + ''.join(list_of_in_deg_as_str)
    id_rev = ''.join(list_of_in_deg_as_str) + ' ' + ''.join(list_of_out_deg_as_str)
    id = id + ' ' + str(current_lin)
    id_rev = id_rev + ' ' + str(current_lin)

    """#color refinement#
    list_of_colors_as_str = list(map(str, sorted(color_refinement(new_poset))))
    list_of_colors_rev_as_str = list(map(str, sorted(color_refinement(new_poset_rev))))
    color_id = ''.join(list_of_colors_as_str)
    color_id_rev = ''.join(list_of_colors_rev_as_str)
    id = id + ' ' + color_id
    id_rev = id_rev + ' ' + color_id_rev
    """

    id_in_new_posets = False

    if id not in new_posets and id_rev not in new_posets:  # no poset with either id
        return False, id_in_new_posets, id

    if id in new_posets:  # check for isomorphism, only those with the same id
        id_in_new_posets = True
        for p in new_posets[id]:
            if nx.is_isomorphic(new_poset, p):
                return True, id_in_new_posets, id

    if id_rev in new_posets:  # check for isomorphism of the reverse
        # new_poset_rev = reverse_graph(new_poset)  # reverse of the graph,
        # only needed if reverse not calculated above
        for p in new_posets[id_rev]:
            if nx.is_isomorphic(new_poset_rev, p):
                return True, id_in_new_posets, id

    return False, id_in_new_posets, id


def iso_poset_in_dict(poset, poset_rev, poset_dict):
    """
    Checks wether a given poset, and its reverse are in a given dictionary.
    :param poset: the poset to check
    :param poset_rev: reverse of the poset
    :param poset_dict: the dictionary in which the poset is searched
    :return: is_isomorphic, simple_id of poset
    """
    list_of_out_deg_as_str = list(map(str, sorted(d for n, d in poset.out_degree())))
    list_of_in_deg_as_str = list(map(str, sorted(d for n, d in poset.in_degree())))
    id = ''.join(list_of_out_deg_as_str) + ' ' + ''.join(list_of_in_deg_as_str)
    id_rev = ''.join(list_of_in_deg_as_str) + ' ' + ''.join(list_of_out_deg_as_str)

    if id not in poset_dict and id_rev not in poset_dict:  # no poset with either id
        return False, id

    if id in poset_dict:  # check for isomorphism, only those with the same id
        for p in poset_dict[id]:
            if nx.is_isomorphic(poset, p):
                return True, id

    if id_rev in poset_dict:  # check for isomorphism of the reverse
        for p in poset_dict[id_rev]:
            if nx.is_isomorphic(poset_rev, p):
                return True, id

    return False, id


def add_poset_to_dict(poset, poset_id, dictionary):
    if poset_id not in dictionary.keys():
        dictionary[poset_id] = [poset]
    else:
        dictionary[poset_id].append(poset)


def get_simple_id(poset):
    """
    Creates a simple identifier, that is used for isomorphism check
    :param poset: the poset for which the id should be created
    :return: simple id of the poset
    """
    list_of_out_deg_as_str = list(map(str, sorted(d for n, d in poset.out_degree())))
    list_of_in_deg_as_str = list(map(str, sorted(d for n, d in poset.in_degree())))
    id = ''.join(list_of_out_deg_as_str) + ' ' + ''.join(list_of_in_deg_as_str)
    return id


def draw_graph(poset, lable=''):
    """
    Draws a graph/poset using matplotlib. The figure will have a name specified in lable.
    :param poset: the poset to draw
    :param lable: a string added to the title of the figure
    :return: displays the figure
    """
    plt.figure(lable + ' ' + str(id(poset)))
    nx.draw(poset, with_labels=True)
    plt.show()


def is_sorted(poset):
    """
    checks wether a poset is a linear order, by comparing the simple_id with the expected id of a linear order.
    :param poset: the poset to check
    :return: True if the poset is sorted/ a linear order
    """
    n = len(poset.nodes)
    string_of_sorted = '0' + '1' * (n - 1) + ' ' + '0' + '1' * (n - 1)
    return get_simple_id(poset) == string_of_sorted
