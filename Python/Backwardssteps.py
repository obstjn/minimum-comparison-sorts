import networkx as nx
import numpy as np
import Graphtools as gt
import LinExtensions as le
import Forwardsteps
import sys, traceback
import time
import cProfile

start_time = time.time()

n = 7
C = 13
S_star_dict = [{} for i in range(C + 1)]
S_comp_dict = [{} for i in range(C + 1)]

lin_order = nx.DiGraph()  # Linear order
lin_order.add_nodes_from(range(n))
lin_order.add_path(range(n))
S_star_dict[0][gt.get_simple_id(lin_order)] = [lin_order]  # posets as dicts


def backwardsteps(S, c_left, print_progress=True):

    global S_star_dict, S_comp_dict, n, lin_order
    S_star = [[] for i in range(c_left + 1)]
    S_star[-1].append(lin_order)
    current_S_star = {}  # posets of S[c + 1]
    current_S_star[gt.get_simple_id(lin_order)] = [lin_order]

    for c in range(1, c_left + 1):  # c is the number of remaining comparisons
        if not S[c_left - c] or (len(S[c_left - c]) == 1 and gt.is_sorted(S[c_left - c][0])):
            S_star[c_left - c].append(lin_order)
            continue

        new_S_star = {}  # storing posets from S that can be sorted in C -c comparisons
        i = 0  # for analysing purposes
        for poset in S[c_left - c]:
            #gt.draw_graph(poset, "S[{}]".format(c_left - c))
            i += 1
            matrix = nx.to_numpy_array(nx.transitive_closure(poset), dtype=int)
            related = np.logical_or(matrix, np.transpose(matrix))  # find the related Pairs of the poset
            poset_id = gt.get_simple_id(poset)
            poset_added = False

            num_singletons = 0  # calculate the number of singletons so that we safe comparisons
            for t in range(n):
                if not any(matrix[t]) and not any(matrix[:, t]):
                    num_singletons += 1
            if num_singletons > 2:
                bound = num_singletons - 2
            else:
                bound = 0
            singleton_comp = False  # to safe some singleton comparisons

            for j in range(n - 1 - bound):
                if num_singletons > 1 and j == n - bound - 2:  # only True if last j
                    singleton_comp = True
                for k in range(j + 1, n - bound):
                    if num_singletons > 1 and k == n - bound - 1 and not singleton_comp:  # last k
                        continue  # skip comparisons with the 2nd singleton

                    if related[j, k]:
                        continue
                    else:
                        p1 = poset.copy()
                        p2 = poset.copy()
                        p1.add_edge(j, k)
                        p1, p1_rev = gt.trans_reduction_and_reverse(p1, edge=(j, k))
                        p2.add_edge(k, j)
                        p2, p2_rev = gt.trans_reduction_and_reverse(p2, edge=(k, j))

                        p1_sortable, id1 = gt.iso_poset_in_dict(p1, p1_rev, current_S_star)  # check if sortable
                        p2_sortable, id2 = gt.iso_poset_in_dict(p2, p2_rev, current_S_star)


                        if not p1_sortable and not p2_sortable:  # neither is sortable
                            gt.add_poset_to_dict(poset, poset_id, S_comp_dict[c])
                            continue


                        if p2_sortable and not p1_sortable:
                            #print("S[{}]: p_1: {} p2: {}".format(c_left - c, p1_sortable, p2_sortable))
                            p1_sortable, valid = sortability_dict(p1, p1_rev, c - 1)
                            if not valid:
                                p1_sortable = sortable(p1, c - 1)


                        elif p1_sortable and not p2_sortable:
                            #print("S[{}]: p_1: {} p2: {}".format(c_left - c, p1_sortable, p2_sortable))
                            p2_sortable, valid = sortability_dict(p2, p2_rev, c - 1)

                            if not valid:  # p2 is not sortable
                                p2_sortable = sortable(p2, c - 1)


                        should_add_poset = p1_sortable and p2_sortable

                        if should_add_poset:
                            if poset_id not in new_S_star:
                                new_S_star[poset_id] = [poset]  # new id that is not in new_S_star
                                S_star[c_left - c].append(poset)
                            else:
                                new_S_star[poset_id].append(poset)  # add poset to those with same id
                                S_star[c_left - c].append(poset)
                            poset_added = True  # If the poset was added we need not to check further pairs
                            break

                if poset_added:
                    break

            if print_progress:
                global start_time
                n_c = "\rn = {}, c = {}/{} ".format(n, c, c_left)
                p_in_s = "#Posets in S[{}]: {}/{}\t".format(c_left - c, i, len(S[c_left - c]))
                time_passed = "Time passed: {}s".format(int(time.time() - start_time))
                # poset_time = "\tPosetOpTime: {}ms".format(int((time_pos_f - time_pos_s) * 1000))
                print(n_c + p_in_s + time_passed, end=' ')

        current_S_star = new_S_star
        S_star_dict[c].update(new_S_star)  # merge existing sortable posets in c comparisons with new ones
    return S_star


sort_dict = {}

def sortability_dict(poset, poset_rev, comps):
    """
    Checks if a given poset has already been sorted in a certain number of comparisons.
    :param poset: the poset
    :param poset_rev: reverse of the poset
    :param comps: number of comparisons availible
    :return: (result, validity) result says if the poset can be sorted with that number of comparisons.
            validity states, wether or not the result is valid, i.e. False means the poset is not in the dict.
    """
    global sort_dict

    list_of_out_deg_as_str = list(map(str, sorted(d for n, d in poset.out_degree())))
    list_of_in_deg_as_str = list(map(str, sorted(d for n, d in poset.in_degree())))
    id = ''.join(list_of_out_deg_as_str) + ' ' + ''.join(list_of_in_deg_as_str)
    id_rev = ''.join(list_of_in_deg_as_str) + ' ' + ''.join(list_of_out_deg_as_str)

    if id not in sort_dict and id_rev not in sort_dict:  # no poset with either id
        return False, False

    if id in sort_dict:  # check for isomorphism, only those with the same id
        for p, c in sort_dict[id]:
            if nx.is_isomorphic(poset, p):
                print("match", c<= comps)
                return c <= comps, True

    if id_rev in sort_dict:  # check for isomorphism of the reverse
        for p, c in sort_dict[id_rev]:
            if nx.is_isomorphic(poset_rev, p):
                print("match", c <= comps)
                return c <= comps, True

    return False, False


def sortable(poset, c_left):
    """
    Checks if a given poset that we got after c Comparisons can be sorted in the remaining C-c Comparisons
    :param poset: the poset to check
    :param c: the comparisons that have been made so far
    :param C: the total number of Comparisons that can be made
    :return: True if sortable, else False
    """
    if gt.is_sorted(poset):
        return True
    elif c_left == 0:
        return False

    S = Forwardsteps.forwardsteps(poset, c_left, print_progress=False)

    last_poset = None
    for i in range(len(S) - 1, -1, -1):  # find the last poset in S
        if not S[i]:  # S[i] is empty
            c_left -= 1
            continue
        else:
            last_poset = S[i][0]
            break

    if not gt.is_sorted(last_poset):  # last poset is no linear order
        return False

    S_star = backwardsteps(S, c_left, print_progress=False)

    if S_star[0]:
        gt.add_poset_to_dict((poset, c_left), gt.get_simple_id(poset), sort_dict)
        return True
    else:
        return False


def start_sortability_test(n_, C_):
    """
    Starts the test to check if n_ elements can be sorted in C_ comparisons.
    :param n_: number of elements
    :param C_: number of maximum comparisons
    :return: True if n_ elements can be sorted in C_ comparisons
    """
    global n, C
    n = n_
    C = C_
    a = nx.DiGraph()
    a.add_nodes_from(range(n))

    S = Forwardsteps.forwardsteps(a, C)
    if S[-1]:
        print("\nForwardsteps completed --> Possible!")
        print("Backwardssteps:")
    else:
        print("Can't! sort {} Elements in {} comparisons!".format(n, C))
        return


    global S_star_dict, S_comp_dict
    S_star_dict = [{} for i in range(C + 1)]
    S_comp_dict = [{} for i in range(C + 1)]

    S_star = backwardsteps(S, c_left=C)

    if S_star[0]:
        print("\nSortable!")
    else:
        print("Can't! sort {} Elements in {} comparisons!".format(n, C))


def main_method():
    global n, C
    start_sortability_test(n, C)

    """a = nx.DiGraph()
    a.add_nodes_from(range(n))
    a.add_edges_from([(0, 1), (1, 2), (2, 4), (2, 3)])
    print(sortable(a, c_left=1))"""

    # TODO Backwards steps "transparenter machen"

if __name__=='__main__':
    #main_method()
    cProfile.run("main_method()")

