#include <iostream>
#include <iterator>
#include <algorithm>
#include <utility>      // for std::pair
#include <math.h>       //for log
#include "PosetObj.h"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_iterator.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/vf2_sub_graph_iso.hpp>
#include <boost/graph/transitive_closure.hpp>
#include <chrono>
#include <thread>
#include <mutex>
#include <fstream>


int main()
{
    using namespace boost;
    std::cout << std::boolalpha;
    typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;

/*********************************************/
/**real start of main**/

    int N = 13;
    int C = 33;
    std::cout << "N = "; //activate for user input
    std::cin >> N;
    std::cout << "C = ";
    std::cin >> C;
    std::cout << std::endl;


    //Forward- & Backwardsteps
    std::vector<int> itl(21);
    for(unsigned int i=0; i<itl.size(); i++){
        itl[i] = ceil(log2(PosetObj::factorial(i)));
    }
    //set to false to display output in console
    bool writeToFile = true;

    std::ofstream out("output.txt"); //output file

    //Forwardsteps
    std::cout << "Forwardsteps n = " << N << ", C = " << C << std::endl << std::endl;
    Graph g0(N);
    PosetObj start(g0);

    auto startTime = std::chrono::steady_clock::now();
    std::vector<std::list<PosetObj>> S = start.forwardsteps_MultiThread(C);
    //timing
    auto currentTime = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();

    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buffer
    if(writeToFile) std::cout.rdbuf(out.rdbuf()); //redirect output to file

    std::cout << "\n" << N <<" elements maybe sortable in "<< C << " comparisons: " << !S.back().empty() << std::endl;
    std::cout << "Time Forwardsteps: " << duration << "ms" << std::endl;

    for(unsigned int i=0; i<S.size(); i++){
        std::cout << "Posets in S[" << i << "]: " << S[i].size() << std::endl;
    }

    if(writeToFile) std::cout.rdbuf(coutbuf); //reset output to standard

    //Backwardsteps
    if(!S.back().empty()){
        std::cout << "\n\n############## Backwardsteps ##############\n";
        PosetObj linOrder = PosetObj::CreateLinOrder(N);

        //storing all checked posets
        PosetObj::S_starMap.resize(C+1); //S_starMap[0] contains linorder
        PosetObj::S_compMap.resize(C+1); //S_compMap[1] contains order that needs more than 1 comparison
        PosetObj::S_starMap[0][linOrder.GetidTag()].push_back(linOrder);

        auto backwStartTime = std::chrono::steady_clock::now();
        std::vector<std::list<PosetObj>> S_star = linOrder.backwardsteps(C, S);
        //timing
        auto currentTime2 = std::chrono::steady_clock::now();
        auto durationBackw = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime2-backwStartTime).count();
        auto durationTotal = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime2-startTime).count();

        if(writeToFile) std::cout.rdbuf(out.rdbuf()); //redirect output
        std::cout << "\n" << N <<" elements definitly sortable in "<< C << " comparisons: " << !S_star[0].empty() << std::endl;
        std::cout << "Time Backwardsteps: " << durationBackw << "ms" << std::endl;

        for(unsigned int i=0; i<S_star.size(); i++){ //print sizes of S*
            std::cout << "Posets in S*[" << i << "]: " << S_star[i].size() << std::endl;
        }
        std::cout << "\nTime Total: " << durationTotal << "ms" << std::endl;
        if(writeToFile) std::cout.rdbuf(coutbuf); //reset outout to standard
    }

    return 0;
}

