#include <iostream>
#include "PosetObj.h"
#include <queue>
#include <list>
#include <unordered_map>
#include <algorithm>
#include <chrono>
#include <thread>
#include <set>
#include <boost/dynamic_bitset.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/reverse_graph.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/vf2_sub_graph_iso.hpp>
#include <boost/graph/transitive_closure.hpp>

PosetObj::PosetObj(Graph p_graph){
    //ctor
    graph = p_graph;
    boost::copy_graph(boost::make_reverse_graph(p_graph), revGraph);
    n = num_vertices(graph);
    linExtensions = 0;
    linExtTable.resize(n, std::vector<long long int>(n));
    //addedEdge;
    //idTag;
    //idRevTag;
    //singletons;
    upperlimit = -1;
    lowerlimit = -1;
}

PosetObj::~PosetObj(){
    //dtor
}

PosetObj::PosetObj(const PosetObj& other){
    //copy ctor
    graph = other.Getgraph();
    revGraph = other.GetrevGraph();
    n = other.Getn();
    linExtensions = other.GetlinExtensions();
    linExtTable = other.GetlinExtTable();
    addedEdge = other.addedEdge;
    idTag = other.GetidTag();
    idRevTag = other.GetidRevTag();
    singletons = other.Getsingletons();
    upperlimit = other.Getupperlimit();
    lowerlimit = other.Getlowerlimit();
}

//initzializing static variable
std::vector<std::unordered_map<std::string, std::list<PosetObj>>> PosetObj::S_starMap; //contains at index [i] posets sortable in i comparisons
std::vector<std::unordered_map<std::string, std::list<PosetObj>>> PosetObj::S_compMap; //contains at index [i] posets not sortable in i comparisons
std::mutex PosetObj::atomLock;

/**
 * Adds an edge to the PosetObj i.e. its graph and revGraph.
 *
 * @param j The source of the edge
 * @param k The target of the edge
 */
void PosetObj::add_edge(int j, int k){
    boost::add_edge(j, k, graph);
    boost::add_edge(k, j, revGraph);

    this->addedEdge = std::pair<int, int>(j, k);
    //invalidates singletons, linExtensions, linExtTable, idTag
}

/**
 * Removes an edge of the PosetObj i.e. its graph and revGraph.
 * Does nothing if the edge doesn't exist.
 *
 * @param j The source of the edge
 * @param k The target of the edge
 */
void PosetObj::remove_edge(int j, int k){
    boost::remove_edge(j, k, graph);
    boost::remove_edge(k, j, revGraph);
    //invalidates singletons, linExtensions, linExtTable, idTag
}

/**
 * Gives all adjacent vertices of a node.
 *
 * @param node The node/source to the vertices.
 * @return adjacent A list of all adjacent vertices.
 */
std::vector<int> PosetObj::GetAdjacentVertices(int node){
    std::vector<int> adjacent;
    auto adj_pair = boost::adjacent_vertices(node, this->graph);
    for(auto nbr=adj_pair.first; nbr!=adj_pair.second; nbr++){
        adjacent.push_back(*nbr);
    }
    return adjacent;
}

/**
 * Prints the graph of the Poset in adjacency list represantation.
 */
void PosetObj::print_poset(){
    boost::print_graph(this->Getgraph());
}

/**
 * Prints the last added edge.
 */
void PosetObj::printAddedEdge(){
    std::cout << "(" << this->addedEdge.first << ", " << this->addedEdge.second << ")" << std::endl;
}

/**
 * Creates a poset which is a linear order
 *
 * @param n The number of elements of the linear order
 *
 * @return linOrder A poset that is a linear order.
 */
PosetObj PosetObj::CreateLinOrder(int n){
    Graph g(n);
    PosetObj linOrder(g);

    for(int i=0; i < n-1; i++){
        linOrder.add_edge(i, i+1);
    }
    linOrder.SetlinExtensions(1LL);
    std::string degrees(n, '1'); //string of length n with all 1s
    degrees.replace(0, 1, "0"); //replace first with 0
    std::string linIdTag = degrees + " " + degrees + " 1";
    linOrder.SetidTag(linIdTag);
    linOrder.SetidRevTag(linIdTag);
    linOrder.Setupperlimit(0);
    linOrder.Setlowerlimit(0);

    return linOrder;
}

/**
 * Returns a sorted vector with all successors of a node using BFS.
 *
 * @param node The node in question.
 * @return sorted vector with unique values.
 */
std::vector<int> PosetObj::GetSuccessors(int node){
    std::vector<int> successors;
    std::queue<int> que;
    std::vector<bool> visited(n, false);
    que.push(node);

    while(!que.empty()){
        int currentNode = que.front();
        que.pop();

        auto adjPair = boost::adjacent_vertices(currentNode, graph);
        for(auto iter = adjPair.first; iter != adjPair.second; iter++){
            if(!visited[*iter]){
                que.push(*iter);
                successors.push_back(*iter);
                visited[*iter] = true;
            }
        }
    }

    std::sort(successors.begin(), successors.end());
    return successors;
}

/**
 * Returns a sorted vector with all predecessors of a node using BFS.
 *
 * @param node The node in question.
 * @return sorted vector with unique values.
 */
std::vector<int> PosetObj::GetPredecessors(int node){
    std::vector<int> predecessors;
    std::queue<int> que;
    std::vector<bool> visited(n, false);
    que.push(node);

    while(!que.empty()){
        int currentNode = que.front();
        que.pop();

        auto inv_adjPair = boost::inv_adjacent_vertices(currentNode, graph);
        for(auto iter = inv_adjPair.first; iter != inv_adjPair.second; iter++){
            if(!visited[*iter]){
                que.push(*iter);
                predecessors.push_back(*iter);
                visited[*iter] = true;
            }
        }
    }

    std::sort(predecessors.begin(), predecessors.end());
    return predecessors;
}

/**
 * Calculates and sets idTag and idRevTag.
 * idTag = (sorted) outDegree sequence + inDegree sequence + linearExtensions.
 */
void PosetObj::calculateAndSetIDs(){
    //calculate degree sequence
    std::vector<int> outDegrees(this->n);
    std::vector<int> inDegrees(this->n);
    for(int node=0; node < this->n; node++){
        outDegrees[node] = boost::out_degree(node, this->graph);
        inDegrees[node] = boost::in_degree(node, this->graph);
    }
    std::sort(outDegrees.begin(), outDegrees.end());
    std::sort(inDegrees.begin(), inDegrees.end());

    std::stringstream outDegStrStream;
    std::stringstream inDegStrStream;
    for(int i=0; i< this->n; i++){
        outDegStrStream << outDegrees[i];
        inDegStrStream << inDegrees[i];
    }
    //id = (sorted) outDegree sequence + inDegree sequence + linearExtensions
    this->SetidTag(outDegStrStream.str() + " " + inDegStrStream.str() + " " + std::to_string(this->GetlinExtensions()));
    this->SetidRevTag(inDegStrStream.str() + " " + outDegStrStream.str() + " " + std::to_string(this->GetlinExtensions()));
}

/**
 * Transforms the graph of PosetObj to a reduced one TR,
 * such that there is a path from u to v in graph iff there is one in TR
 * and the number of edges in TR is minimal.
 * The graph needs to be in this form (reduced) before the edge (outNode, inNode) is added.
 *
 * @param outNode the source of the newly added edge
 * @param inNode the target of the newly added edge
 */
void PosetObj::transReduction(int outNode, int inNode){
    Graph& _graph_ = graph;

    std::vector<int> succOfIn = this->GetSuccessors(inNode);
    std::vector<int> predOfOut = this->GetPredecessors(outNode);

    for(int nbr : this->GetAdjacentVertices(outNode)){
        if (std::binary_search(succOfIn.begin(), succOfIn.end(), nbr)){ //if nbr in succOfIn
            this->remove_edge(outNode, nbr);
        }
    }
    for (int pred : predOfOut){ //for correctness predOfOut should include Out (case edge(out, in) needs to be removed)
        if (boost::edge(pred, inNode, _graph_).second){ //if has edge(pred, inNode)
            this->remove_edge(pred, inNode);
            continue; //since any futher relation to succ means the graph wasn't reduced beforehand.
        }
        for(int succ : succOfIn){
            //if can be removed, doesn't need to
            if (boost::edge(pred, succ, _graph_).second){ //if has edge(pred, succ)
                this->remove_edge(pred, succ);
            }
        }
    }
}

/**
 * Computes the transitive closure of the graph of the PosetObj.
 */
void PosetObj::transClosure(){
    Graph TC;
    boost::transitive_closure(graph, TC);
    this->Setgraph(TC);
}

int PosetObj::numberOfUnrelatedPairs(){
    Graph TC;
    boost::transitive_closure(graph, TC);
    int totalOut=0;
    for(int i=0; i<n; i++){
        totalOut += boost::out_degree(i, TC);
    }
    return (n*(n-1)/2)-totalOut;
}

template <typename Graph1, typename Graph2> //used for isomorphism test, since vf2_graph_iso(g1, g2, callback).
struct vf2_simple_callback {

    vf2_simple_callback(const Graph1& graph1, const Graph2& graph2)
    : graph1_(graph1), graph2_(graph2) {}

    template <typename CorrespondenceMap1To2, typename CorrespondenceMap2To1>
    bool operator()(CorrespondenceMap1To2 f, CorrespondenceMap2To1) const {
        return true;
    }

private:
    const Graph1& graph1_;
    const Graph2& graph2_;
};

/**
 * States wether the graphs of two PosetObjs are isomorphic.
 *
 * @param this The first Poset
 * @param other The second Poset
 * @return true iff graphs are isomorphic
 */
bool PosetObj::is_isomorphic(PosetObj& other){
    typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS> Graph;

    Graph g1 = this->Getgraph();
    Graph g2 = other.Getgraph();
    if(this->GetnumSingletons() > 0){ //remove singletons
        for(int node=0; node < this->Getn(); node++){
            if (boost::out_degree(node, g1) == 0 && boost::in_degree(node, g1) == 0){
                boost::remove_vertex(node, g1);
            }
            if (boost::out_degree(node, g2) == 0 && boost::in_degree(node, g2) == 0){
                boost::remove_vertex(node, g2);
            }
        }
    }

    vf2_simple_callback<Graph, Graph> callback(g1, g2);
    return boost::vf2_graph_iso(g1, g2, callback);
}

/**
 * States wether the reverseGraph of a PosetObj and graph of another PosetObj are isomorphic.
 *
 * @param poset1 The first Poset
 * @param poset2 The second Poset
 * @return true iff graphs are isomorphic
 */
bool PosetObj::is_rev_isomorphic(PosetObj& other){
    typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS> Graph;

    Graph g1 = this->revGraph;
    Graph g2 = other.Getgraph();
    if(this->GetnumSingletons() > 0){ //remove singletons
        for(int node=0; node < this->Getn(); node++){
            if (boost::out_degree(node, g1) == 0 && boost::in_degree(node, g1) == 0){
                boost::remove_vertex(node, g1);
            }
            if (boost::out_degree(node, g2) == 0 && boost::in_degree(node, g2) == 0){
                boost::remove_vertex(node, g2);
            }
        }
    }

    vf2_simple_callback<Graph, Graph> callback(g1, g2);
    return boost::vf2_graph_iso(g1, g2, callback);
}

/**
 * Answers wether or not there is a congruent poset in posetMap and returns that poset.
 * return value is a pair of the type pair<bool, PosetObj&>.
 * Two PosetObjs are congruent if their graphs are isomorphic
 * or the reverse graph of the first is isomorphic to the graph of the second.
 * Sets the values "idTag" and "idTagRev".
 *
 * @param posetMap The container that may contain congruent posets to "this" PosetObj.
 * @return (true, isoPosetObj) iff there is a congruent poset in posetMap
 *         (false, this) otherwise
 */
std::pair<bool, PosetObj&> PosetObj::GetCongruentIn(std::unordered_map<std::string, std::list<PosetObj>>& posetMap){
    //calculate degree sequence
    this->calculateAndSetIDs();

    //check if id and idRev not in posetMap
    if(!posetMap.count(this->idTag) && !posetMap.count(this->idRevTag)){
        return std::pair<bool, PosetObj&>(false, *this);
    }
    //check if normal isomorphic
    if (posetMap.count(this->idTag)){
        for(PosetObj& poset : posetMap[this->idTag]){
            if(this->is_isomorphic(poset)){
                return std::pair<bool, PosetObj&>(true, poset);
            }
        }
    }
    //check if reverse isomorphic
    if (posetMap.count(this->idRevTag)){
        for(PosetObj& poset : posetMap[this->idRevTag]){
            if(this->is_rev_isomorphic(poset)){
                return std::pair<bool, PosetObj&>(true, poset);
            }
        }
    }
    //no isomorphic found
    return std::pair<bool, PosetObj&>(false, *this);
}

/**
 * Answers wether or not there is a congruent poset in posetMap.
 * Two PosetObjs are congruent if their graphs are isomorphic
 * or the reverse graph of the first is isomorphic to the graph of the second.
 * Sets the values "idTag" and "idTagRev".
 *
 * @param posetMap The container that may contain congruent posets to "this" PosetObj.
 * @return true iff there is a congruent poset in posetMap
 */
bool PosetObj::has_congruent_in(std::unordered_map<std::string, std::list<PosetObj>>& posetMap){
    return this->GetCongruentIn(posetMap).first;
}

/**
 * Simple Method to check if there is a congruent poset in a list.
 * Checks normal isomorphism and reverse isomorphism.
 *
 * @param posetList The list that is searched for isomorphic Posets.
 * @return true if a congruent poset was found, false else.
 */
bool PosetObj::has_congruent_in_list(std::list<PosetObj>& posetList, std::list<PosetObj>& posetRevList){
    if(posetList.empty() && posetRevList.empty()){
        return false;
    }
    //check if normal isomorphic
    for(PosetObj& poset : posetList){
        if(this->is_isomorphic(poset)){
            return true;
        }
    }
    //check if rev isomorphic
    for(PosetObj& poset : posetRevList){
        if(this->is_rev_isomorphic(poset)){
            return true;
        }
    }
    //no isomorphic found
    return false;
}

/**
 * Returns the isomorphic PosetObj from a poset map, iff the map contains such a poset.
 *
 * @param posetMap The map to be searched for isomorphic posets
 * @return Isomorphic PosetObj if there is one, else returns NULL
 */
PosetObj& PosetObj::GetMatchFrom(std::unordered_map<std::string, std::list<PosetObj>>& posetMap){
    return this->GetCongruentIn(posetMap).second;
}

/**
 * Returns the number of linear extensions (ways a given poset can be topologically sorted) e_p and sets the table t,
 * with t[j,k] = P + ujuk i.e. the number of linear extensions if the relation ujuk is added to the poset.
 * Calculation done with a graph in adjacency list representation.
 *
 * @return e_p Number of linear extensions
 *
 * References
 * ----------
 * ..[1] M. Peczarski. Towards optimal sorting of 16 elements. CoRR, abs/1108.0866, 2011
 *   https://arxiv.org/pdf/1108.0866.pdf
 */
long long int PosetObj::calculateLinExtensions(){
    int n = this->Getn();
    std::vector<std::vector<long long int>> t;
    t.resize(n, std::vector<long long int>(n));


    Graph& _graph_ = graph;
    //table contains d(V), u(V), visit number for every subset of U
    std::vector<std::vector<long long int>> table(1 << n, std::vector<long long int>(3)); //1 << n == 2**n
    table.front() = {1, 0, 0};
    table.back() = {0, 1, 0};

    std::queue<boost::dynamic_bitset<>> que; //binary numbers representing sets
    boost::dynamic_bitset<> set0(n); //empty set
    que.push(set0);

    int visit = 1; //timestamp

    while(!que.empty()){
        boost::dynamic_bitset<> bstring = que.front();
        //maybe replace by
        //boost::dynamic_bitset<>* bstring = &que.front(); and (*bstring)[0]
        que.pop();

        for(int i=0; i<n; i++){
            //check if nodes (i) not in set can be added to set (starting with lowest node number)
            if(!bstring[i]){
                int node = i; //current node
                bool not_downset = false;

                auto in_pair = inv_adjacent_vertices(node, _graph_); //iterate over ingoing vertices of node
                for(auto in_iter=in_pair.first; in_iter!=in_pair.second; in_iter++){
                    // !bstring[3] is the same as bstring[3] == 0
                    if(!bstring[*in_iter]){
                        //that means with node it's not a downset, since u is a child/predecessor not in the set (==0)
                        not_downset = true;
                        break;
                    }
                }
                if(not_downset){
                    continue;
                } else {
                    boost::dynamic_bitset<> idx = bstring;
                    idx.set(i);

                    if(table[idx.to_ulong()][2] < visit){
                        que.push(idx);
                        table[idx.to_ulong()][2] = visit;
                    }
                    //addition of values
                    table[idx.to_ulong()][0] += table[bstring.to_ulong()][0];
                }
            }
        }
    }

    visit = 2;
    boost::dynamic_bitset<> set1(n);
    set1.set();
    que.push(set1);

    while(!que.empty()){
        boost::dynamic_bitset<> bstring = que.front();
        //maybe replace by
        //boost::dynamic_bitset<>* bstring = &que.front(); and (*bstring)[0]
        que.pop();

        for(int j=0; j<n; j++){
            //check if nodes (j) in the set can be removed from set
            if(bstring[j]){
                int node = j; //current node
                bool not_downset = false;

                auto out_pair = adjacent_vertices(node, _graph_); //iterate over outgoing vertices of node
                for(auto out_iter=out_pair.first; out_iter!=out_pair.second; out_iter++){
                    // bstring[3] is the same as bstring[3] == 1
                    if(bstring[*out_iter]){
                        //that means with node it's not a downset, since u is a parent/successor in the set (==1)
                        not_downset = true;
                        break;
                    }
                }
                if(not_downset){
                    continue;
                } else {
                    boost::dynamic_bitset<> idx = bstring;
                    idx.reset(j);

                    if(table[idx.to_ulong()][2] < visit){
                        que.push(idx);
                        table[idx.to_ulong()][2] = visit;
                    }
                    //addition of values
                    table[idx.to_ulong()][1] += table[bstring.to_ulong()][1];
                    //calculation of t[j,k]
                    long long int d_v = table[idx.to_ulong()][0];
                    long long int u_w = table[bstring.to_ulong()][1];

                    for(int k=node+1 ; k<n; k++){ //only fill upper triangle of t
                        //checks if u_k not in W
                        if(!bstring[k]){
                            t[node][k] += d_v * u_w;
                        }
                    }
                }
            }
        }
    }
    long long int e_p = table.back()[0];
    for(int i=1; i < n; i++){ //calculate lower triangle
        for(int j=0; j<i; j++){
            t[i][j] = e_p - t[j][i];
        }
    }
    this->SetlinExtensions(e_p);
    this->SetlinExtTable(t);
    //linExtTable is also set due to reference
    return e_p;
}

/**
 * Calculates the number of linear extensions on a graph, by reducing it to contain 2 singletons at max
 * and computes for the the missing values for t[j,k] in a faster way.
 * Uses calculateLinExtensions() for the reduced graph.
 *
 * @return e_p Number of linear extensions
 */
long long int PosetObj::calculateLinExtensionsSingleton(){
    int n = this->Getn();
    std::list<int> singleton_list;
    Graph& _graph_ = graph;

    //calculate singletons
    auto nodes_pair = boost::vertices(_graph_);
    for(auto iter=nodes_pair.first; iter!=nodes_pair.second; iter++){
        if(boost::out_degree(*iter, _graph_) == 0 && boost::in_degree(*iter, _graph_) == 0){
            singleton_list.push_back(*iter);
        }
    }
    this->Setsingletons(singleton_list);

    if(this->GetnumSingletons() <= 1){
        return this->calculateLinExtensions(); //sets linExtensions and linExtTable
    }

    //remove singletons
    std::list<int> removable_singletons = singleton_list;
    removable_singletons.pop_front(); //remove all but the first singleton
    Graph reduced_graph = this->Getgraph();
    for(int node : removable_singletons){
        boost::remove_vertex(node, reduced_graph);
    }

    //calculate with reduced Poset
    PosetObj reduced_PosetObj(reduced_graph);
    long long int e_p = reduced_PosetObj.calculateLinExtensions();

    int k = this->GetnumSingletons();
    long long int fac = factorial(n) / factorial(n - k + 1); //one singleton remains --> +1
    e_p *= fac; //adjust values with factorial
    std::vector<std::vector<long long int>> t_reduced = reduced_PosetObj.GetlinExtTable();
    for(unsigned int i=0; i < t_reduced.size(); i++){
        for(unsigned int j=0; j < t_reduced[i].size(); j++){
            t_reduced[i][j] *= fac;
        }
    }

    std::vector<std::vector<long long int>> new_t;
    new_t.resize(n, std::vector<long long int>(n));
    int lastIdx = t_reduced.size() - 1;
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            if(i <= n-k && j <= n-k){ //includes the one singeltons
                new_t[i][j] = t_reduced[i][j]; //copy entry
                continue;
            } else if(i < n-k && j > n-k){
                new_t[i][j] = t_reduced[i][lastIdx]; //last entry of the row
                continue;
            } else if(i > n-k && j < n-k){
                new_t[i][j] = t_reduced[lastIdx][j]; //last entry of the column
                continue;
            } else if(i >= n-k && j >= n-k){
                if(i != j){
                    new_t[i][j] = e_p / 2; //compare two singletons
                } else {
                    new_t[i][j] = 0;
                }
            }
        }
    }
    this->SetlinExtensions(e_p);
    this->SetlinExtTable(new_t);
    return e_p;
}

/**
 * This Method can be given to a thread to calculate the linear extensions of poset.
 *
 * @param poset The PosetObj for which the liner extensions should be calculated.
 */
void PosetObj::parallelLinExtensions(PosetObj& poset){
    poset.calculateLinExtensionsSingleton();
}

/**
 * Forwardsteps to calculate wether this Poset can be sorted in cLeft comparisons.
 * Returning the set S[0,...,cLeft], S[i] contains Posets that are possibly sortable in cLeft-i comparisons.
 * If S[cleft] is empty, this PosetObj can't be sorted in cLeft comparisons.
 * Otherwise Backwardsteps need to be employed.
 *
 * @param cLeft Integer denoting the comparisons left to sort this PosetObj.
 * @param printProgress flag that prints the progress of the calculation if it's set to true.
 *
 * @return S The set containing the posets created during the calculation.
 */
std::vector<std::list<PosetObj>> PosetObj::forwardsteps(int cLeft, bool printProgress){
    std::vector<std::list<PosetObj>> S;
    S.resize(cLeft + 1);
    if(cLeft > 0){
        S[0].push_back(*this);
    }

    auto startTime = std::chrono::steady_clock::now();

    for(int c=1; c < cLeft+1; c++){
        std::unordered_map<std::string, std::list<PosetObj>> new_posets;
        long long int limit = 1LL << (cLeft-c);
        int pNumber = 0; //to track how much posets have been tested

        for(PosetObj& currentPoset : S[c - 1]){
            pNumber++;
            currentPoset.calculateLinExtensionsSingleton();

            int bound = 0;
            int currentNumSingletons = currentPoset.GetnumSingletons();
            if(currentNumSingletons > 2){
                bound = currentNumSingletons - 2;
            }
            bool singleton_comp = false; //to safe singleton comparisons

            for(int j=0; j < n-bound-1; j++){
                if(currentNumSingletons > 1 && j == n-bound-2){ //only true for last j
                    singleton_comp = true;
                }
                for(int k=j+1; k<n-bound; k++){ //checks all combinations of ujuk until 2 singletons
                    if(currentNumSingletons > 1 && k==n-bound-1 && !singleton_comp){ //last k
                        continue; //skip comparisons with 2nd singleton
                    }
                    long long int p_1 = currentPoset.linExtTable[j][k];
                    long long int p_2 = currentPoset.linExtTable[k][j];

                    if(p_1 == 0 || p_2 == 0){ //comparing already related pair
                        continue;
                    }
                    if(p_1 > limit || p_2 > limit){ //not sortable in remaining comparisons
                        continue;
                    } else {
                        PosetObj new_poset_p1 = currentPoset; //copying
                        PosetObj new_poset_p2 = currentPoset;

                        if(p_1 >= p_2){ //add poset with greater number of linear extensions
                            new_poset_p1.add_edge(j, k);
                            new_poset_p1.transReduction(j, k);
                            new_poset_p1.SetlinExtensions(p_1);

                            new_poset_p2.add_edge(k, j);
                            new_poset_p2.transReduction(k, j);
                            new_poset_p2.SetlinExtensions(p_2);
                        } else {
                            new_poset_p1.add_edge(k, j);
                            new_poset_p1.transReduction(k, j);
                            new_poset_p1.SetlinExtensions(p_2);

                            new_poset_p2.add_edge(j, k);
                            new_poset_p2.transReduction(j, k);
                            new_poset_p2.SetlinExtensions(p_1);
                        }

                        if(new_poset_p1.has_congruent_in(new_posets)){
                            continue; //found congruent poset
                        } else if(new_poset_p2.has_congruent_in(new_posets)){
                            continue; //
                        } else {
                            new_posets[new_poset_p1.GetidTag()].push_back(new_poset_p1);
                            S[c].push_back(new_poset_p1);
                        }
                    }
                }
            }
            auto currentTime = std::chrono::steady_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();

            if(printProgress){
                std::stringstream outLine;
                outLine << "\033[A\33[2K\r";
                outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                outLine << "#Posets in S[" << c-1 << "]: " << pNumber << "/" << S[c-1].size() << "\t";
                outLine << "Time passed: " << duration << "ms";
                std::cout << outLine.str() << std::endl; //remove std::endl on windows
            }
        }
    }

    return S;
}

/**
 * Forwardsteps2 to calculate wether this Poset can be sorted in cLeft comparisons.
 * It uses information, that some posets are not sortable.
 * Used in the recursion of sortable().
 * Returning the set S[0,...,cLeft], S[i] contains Posets that are possibly sortable in cLeft-i comparisons.
 * If S[cleft] is empty, this PosetObj can't be sorted in cLeft comparisons.
 * Otherwise Backwardsteps need to be employed.
 *
 * @param cLeft Integer denoting the comparisons left to sort this PosetObj.
 * @param printProgress flag that prints the progress of the calculation if it's set to true.
 *
 * @return S The set containing the posets created during the calculation.
 */
std::vector<std::list<PosetObj>> PosetObj::forwardsteps2(int cLeft, bool printProgress){
    std::vector<std::list<PosetObj>> S;
    S.resize(cLeft + 1);
    if(cLeft > 0){
        S[0].push_back(*this);
    }

    auto startTime = std::chrono::steady_clock::now();

    for(int c=1; c < cLeft+1; c++){
        std::unordered_map<std::string, std::list<PosetObj>> new_posets;
        long long int limit = 1LL << (cLeft-c);
        int pNumber = 0; //to track how much posets have been tested

        for(PosetObj& currentPoset : S[c - 1]){
            pNumber++;
            currentPoset.calculateLinExtensionsSingleton();

            int bound = 0;
            int currentNumSingletons = currentPoset.GetnumSingletons();
            if(currentNumSingletons > 2){
                bound = currentNumSingletons - 2;
            }
            bool singleton_comp = false; //to safe singleton comparisons

            for(int j=0; j < n-bound-1; j++){
                if(currentNumSingletons > 1 && j == n-bound-2){ //only true for last j
                    singleton_comp = true;
                }
                for(int k=j+1; k<n-bound; k++){ //checks all combinations of ujuk until 2 singletons
                    if(currentNumSingletons > 1 && k==n-bound-1 && !singleton_comp){ //last k
                        continue; //skip comparisons with 2nd singleton
                    }
                    long long int p_1 = currentPoset.linExtTable[j][k];
                    long long int p_2 = currentPoset.linExtTable[k][j];

                    if(p_1 == 0 || p_2 == 0){ //comparing already related pair
                        continue;
                    }
                    if(p_1 > limit || p_2 > limit){ //not sortable in remaining comparisons
                        continue;
                    } else {
                        PosetObj new_poset_p1 = currentPoset; //copying
                        PosetObj new_poset_p2 = currentPoset;

                        if(p_1 >= p_2){ //add poset with greater number of linear extensions
                            new_poset_p1.add_edge(j, k);
                            new_poset_p1.transReduction(j, k);
                            new_poset_p1.SetlinExtensions(p_1);
                            if (new_poset_p1.isDefinitelyNotSortable(cLeft-c)){
                                continue;
                            }

                            new_poset_p2.add_edge(k, j);
                            new_poset_p2.transReduction(k, j);
                            new_poset_p2.SetlinExtensions(p_2);
                            if (new_poset_p2.isDefinitelyNotSortable(cLeft-c)){
                                continue;
                            }
                        } else {
                            new_poset_p1.add_edge(k, j);
                            new_poset_p1.transReduction(k, j);
                            new_poset_p1.SetlinExtensions(p_2);
                            if (new_poset_p1.isDefinitelyNotSortable(cLeft-c)){
                                continue;
                            }

                            new_poset_p2.add_edge(j, k);
                            new_poset_p2.transReduction(j, k);
                            new_poset_p2.SetlinExtensions(p_1);
                            if (new_poset_p2.isDefinitelyNotSortable(cLeft-c)){
                                continue;
                            }
                        }

                        if(new_poset_p1.has_congruent_in(new_posets)){
                            continue; //found congruent poset
                        } else if(new_poset_p2.has_congruent_in(new_posets)){
                            continue; //
                        } else {
                            new_posets[new_poset_p1.GetidTag()].push_back(new_poset_p1);
                            S[c].push_back(new_poset_p1);
                        }
                    }
                }
            }
            auto currentTime = std::chrono::steady_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();

            if(printProgress){
                std::stringstream outLine;
                outLine << "\033[A\33[2K\r";
                outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                outLine << "#Posets in S[" << c-1 << "]: " << pNumber << "/" << S[c-1].size() << "\t";
                outLine << "Time passed: " << duration << "ms";
                std::cout << outLine.str() << std::endl; //remove std::endl on windows
            }
        }
    }

    return S;
}

/**
 * Backwardsteps to calculate wether this Poset can really be sorted in cLeft comparisons.
 * Returning the set S*[0,...,cLeft], S[i] contains Posets that are definitely sortable in cLeft-i comparisons.
 * If S*[0] is empty, this PosetObj can't be sorted in cLeft comparisons.
 *
 * @param cLeft Integer denoting the comparisons left to sort this PosetObj.
 * @param printProgress flag that prints the progress of the calculation if it's set to true.
 *
 * @return S* (S_star) The set containing the posets that can be sorted in cLeft-i comparisons.
 */
std::vector<std::list<PosetObj>> PosetObj::backwardsteps(int cLeft, std::vector<std::list<PosetObj>>& S, bool printProgress){
    std::vector<std::list<PosetObj>> S_star;
    S_star.resize(cLeft + 1);
    S_star.back().push_back(*this); //starting call on linear order, alternatively S.back()[0]

    std::unordered_map<std::string, std::list<PosetObj>> S_starPlus1;
    S_starPlus1[this->GetidTag()].push_back(*this);

    auto startTime = std::chrono::steady_clock::now();

    for(int c=cLeft-1; c >= 0; c--){
        if(S_star[c+1].empty()) return S_star; //not sortable

        std::unordered_map<std::string, std::list<PosetObj>> new_S_star;
        long long int limit = 1LL << (cLeft-c-1);
        int pNumber = 0; //to track poset progress

        for(PosetObj& currentPoset : S[c]){
            bool posetAdded = false;
            pNumber++;

            int bound = 0;
            int currentNumSingletons = currentPoset.GetnumSingletons();
            if(currentNumSingletons > 2){
                bound = currentNumSingletons - 2;
            }
            bool singleton_comp = false; //to safe singleton comparisons

            for(int j=0; j < this->n-bound-1; j++){
                if(currentNumSingletons > 1 && j == n-bound-2){ //only true for last j
                    singleton_comp = true;
                }
                for(int k=j+1; k<n-bound; k++){ //checks all combinations of ujuk until 2 singletons
                    if(currentNumSingletons > 1 && k==n-bound-1 && !singleton_comp){ //last k
                        continue; //skip comparisons with 2nd singleton
                    }

                    long long int p_1 = currentPoset.linExtTable[j][k];
                    long long int p_2 = currentPoset.linExtTable[k][j];

                    if(p_1 == 0 || p_2 == 0){ //comparing already related pair
                        continue;
                    }
                    if(p_1 > limit || p_2 > limit){ //not sortable in remaining comparisons
                        continue;
                    } else {
                        PosetObj poset_p1 = currentPoset; //copying
                        PosetObj poset_p2 = currentPoset;

                        poset_p1.add_edge(j, k);
                        poset_p1.transReduction(j, k);
                        poset_p1.SetlinExtensions(p_1);

                        poset_p2.add_edge(k, j);
                        poset_p2.transReduction(k, j);
                        poset_p2.SetlinExtensions(p_2);

                        bool p1_inSstar = poset_p1.has_congruent_in(S_starPlus1);
                        bool p2_inSstar = poset_p2.has_congruent_in(S_starPlus1);

                        if(!p1_inSstar && !p2_inSstar){ //neither one in S*[c+1]
                            continue;
                        } else if(p1_inSstar && !p2_inSstar){
                            p2_inSstar = poset_p2.sortable(cLeft-c-1);
                        } else if(!p1_inSstar && p2_inSstar){
                            p1_inSstar = poset_p1.sortable(cLeft-c-1);
                        }

                        if(p1_inSstar && p2_inSstar){
                            currentPoset.Setupperlimit(cLeft-c); //needs at max cLeft-c comparisons to be sorted
                            new_S_star[currentPoset.GetidTag()].push_back(currentPoset);
                            S_star[c].push_back(currentPoset);
                            if(!currentPoset.has_congruent_in(S_starMap[cLeft-c])){
                                S_starMap[cLeft-c][currentPoset.GetidTag()].push_back(currentPoset);
                            }
                            posetAdded = true;
                            break;
                        }
                    }
                }
                if(posetAdded){
                    break;
                }
            }
            if (!posetAdded){ //not sortable
                currentPoset.Setlowerlimit(cLeft-c+1); //needs at least cLeft-c+1 comparisons to be sorted
                if(!currentPoset.has_congruent_in(S_compMap[cLeft-c])){
                    PosetObj::S_compMap[cLeft-c][currentPoset.GetidTag()].push_back(currentPoset);
                    //TODO check here ^~~~~~~~
                }
            }

            auto currentTime = std::chrono::steady_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();

            if(printProgress){
                std::stringstream outLine;
                outLine << "\33[A\33[2K\r"; //delete line
                outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                outLine << "S*["<< c <<"]: #Posets in S[" << c << "]: " << pNumber << "/" << S[c].size() << "\t";
                outLine << "Time passed: " << duration << "ms";
                std::cout << outLine.str() << std::endl; //remove std::endl on windows
            }
        }
        S_starPlus1 = new_S_star;
    }

    return S_star;
}

/**
 * Calculates wether or not a given Poset is sortable in cLeft comparisons.
 * Does so using forwardsteps2() and backwardsteps.
 *
 * @param cLeft Number of comparisons to check.
 * @return true if sortable in cLeft comparisons, false otherwise.
 */
bool PosetObj::sortable(int cLeft){
    if(this->GetlinExtensions() <= 6){ //easily sortable
        return true;
    }
    if (this->numberOfUnrelatedPairs() <= cLeft){
        return true;
    }
    // already checked
    if (this->isDefinitelySortable(cLeft)){
        return true;
    } else if(this->isDefinitelyNotSortable(cLeft)){
        return false;
    }
    // two sortable successors
    if(this->has_two_sortable_succ(cLeft)){
        return true;
    }
        //TODO change to forwardsteps2
    std::vector<std::list<PosetObj>> S = this->forwardsteps2_MultiThread(cLeft);
    if(S.back().empty()){ //forwardsteps fail
        this->Setlowerlimit(cLeft+1);
        S_compMap[cLeft][this->GetidTag()].push_back(*this);
        return false;
    } else {
        PosetObj linOrder = PosetObj::CreateLinOrder(this->n);
        std::vector<std::list<PosetObj>> S_star = linOrder.backwardsteps(cLeft, S, false);

        bool sortable_ = !S_star[0].empty();

        if (sortable_){ //store that the poset is sortable
            this->Setupperlimit(cLeft);
            S_starMap[cLeft][this->GetidTag()].push_back(*this);
        } else { //store that the poset is not sortable
            this->Setlowerlimit(cLeft+1);
            S_compMap[cLeft][this->GetidTag()].push_back(*this);
        }

        return sortable_;
    }
}

/**
 * Tells if this poset is definitely not sortable in cLeft comparisons.
 * If this method outputs true, than it is definitely not sortable in cLeft comparisons.
 * If the output is false it might still be not sortable in cLeft comparisons.
 *
 * @param cLeft The number of comparisons left.
 * @return true if definitely not sortable in cLeft
 *         false otherwise.
 */
bool PosetObj::isDefinitelyNotSortable(int cLeft){
    if(this->GetlinExtensions() <= 6){ //easily sortable
        return false;
    }
    int linExt = this->GetlinExtensions();
    int logLinExt = 0;
    while(linExt >>= 1) ++logLinExt; //calculate log2(linExtensions)

    for(unsigned int i=cLeft; i < S_compMap.size(); i++){ //check alredy processed posets
        //TODO check here         ^~~~~~~~~~~~~~~~
        if (this->has_congruent_in(PosetObj::S_compMap[i])){
            return true;
        }
    }
    return false;
}

/**
 * Tells if this poset is definitely sortable in cLeft comparisons.
 * If this method outputs true, than it is definitely sortable in cLeft comparisons.
 * If the output is false it might still be sortable in cLeft comparisons.
 *
 * @param cLeft The number of comparisons left.
 * @return true if definitely sortable in cLeft
 *         false otherwise.
 */
bool PosetObj::isDefinitelySortable(int cLeft){
    if(this->GetlinExtensions() <= 6){ //easily sortable
        return true;
    }
    int linExt = this->GetlinExtensions();
    int logLinExt = 0;
    while(linExt >>= 1) ++logLinExt; //calculate log2(linExtensions)

    for(int i=cLeft; i >=logLinExt; i--){ //check alredy processed posets
        if (this->has_congruent_in(PosetObj::S_starMap[i])){
            return true;
        }
    }
    return false;
}

/**
 * Checks if the Poset is sortable in cLeft,
 * by looking for two successors that are sortable in cLeft-1.
 *
 * @param cLeft number of Comparisions the current Poset has left (not its successors).
 * @return true if there are such two Posets
 *         false otherwise
 */
bool PosetObj::has_two_sortable_succ(int cLeft){
    long long int limit = 1LL << (cLeft-1);

    int bound = 0;
    int currentNumSingletons = this->GetnumSingletons();
    if(currentNumSingletons > 2){
        bound = currentNumSingletons - 2;
    }
    bool singleton_comp = false; //to safe singleton comparisons

    for(int j=0; j < n-bound-1; j++){
        if(currentNumSingletons > 1 && j == n-bound-2){ //only true for last j
            singleton_comp = true;
        }
        for(int k=j+1; k<n-bound; k++){ //checks all combinations of ujuk until 2 singletons
            if(currentNumSingletons > 1 && k==n-bound-1 && !singleton_comp){ //last k
                continue; //skip comparisons with 2nd singleton
            }
            long long int p_1 = this->linExtTable[j][k];
            long long int p_2 = this->linExtTable[k][j];

            if(p_1 == 0 || p_2 == 0){ //comparing already related pair
                continue;
            }
            if(p_1 > limit || p_2 > limit){ //not sortable in remaining comparisons
                continue;
            } else {
                PosetObj new_poset_p1 = *this; //copying
                PosetObj new_poset_p2 = *this;
                bool p1_sortable = false;
                bool p2_sortable = false;

                new_poset_p1.add_edge(j, k);
                new_poset_p1.transReduction(j, k);
                new_poset_p1.SetlinExtensions(p_1);
                if (new_poset_p1.isDefinitelySortable(cLeft-1)){
                    p1_sortable = true;
                }

                new_poset_p2.add_edge(k, j);
                new_poset_p2.transReduction(k, j);
                new_poset_p2.SetlinExtensions(p_2);
                if (new_poset_p2.isDefinitelySortable(cLeft-1)){
                    p2_sortable = true;
                }

                if(p1_sortable && p2_sortable){
                    return true;
                }
            }
        }
    }
    return false;
}

std::vector<std::list<PosetObj>> PosetObj::forwardsteps_MultiThread(int cLeft, bool printProgress){
    std::vector<std::list<PosetObj>> S;
    std::mutex S_mtx;
    S.resize(cLeft + 1);
    if(cLeft > 0){
        S[0].push_back(*this);
    }

    auto startTime = std::chrono::steady_clock::now();

    for(int c=1; c < cLeft+1; c++){
        std::unordered_map<std::string, std::list<PosetObj>> new_posets;
        std::unordered_map<std::string, int> versionCounter;
        std::shared_mutex new_posets_mtx;
        long long int limit = 1LL << (cLeft-c);

        std::vector<std::thread> threads;
        int maxActiveThreads = std::thread::hardware_concurrency()*5;
        threads.resize(maxActiveThreads); //max 6*5 Threads
        int idx = 0;
        int posetProgress = 0;
        int pNumber = 0; //only Singlethread relevant

        for(PosetObj& currentPoset : S[c - 1]){
            //thread start
            threads[idx] = std::thread(PosetObj::parallelLinExtensions, std::ref(currentPoset));
            idx++;

            if(idx >= maxActiveThreads){
                posetProgress += maxActiveThreads;
                idx = 0;

                for(int i=0; i<maxActiveThreads; i++){
                    threads[i].join();

                    if(printProgress){
                        auto currentTime = std::chrono::steady_clock::now();
                        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();
                        std::stringstream outLine;
                        outLine << "\033[A\33[2K\r";
                        outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                        outLine << "LinExtensions: " << i + posetProgress << "/" << S[c-1].size() << "\t";
                        outLine << "Time passed: " << duration << "ms";
                        std::cout << outLine.str() << std::endl; //remove std::endl on windows
                    }
                }
            }
        }
        for(int i=0; i<idx; i++){
            threads[i].join();

            if(printProgress){
                auto currentTime = std::chrono::steady_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();
                std::stringstream outLine;
                outLine << "\033[A\33[2K\r";
                outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                outLine << "LinExtensions: " << i + posetProgress << "/" << S[c-1].size() << "\t";
                outLine << "Time passed: " << duration << "ms";
                std::cout << outLine.str() << std::endl; //remove std::endl on windows
            }
        }

        idx = 0;
        posetProgress = 0;
        for(PosetObj& currentPoset : S[c - 1]){

//            PosetObj::posetForward(currentPoset, limit, new_posets, new_posets_mtx, versionCounter); //activate this line for singlethread
//            if(printProgress){                                                                       //activate this block for singlethread
//                pNumber++;
//                auto currentTime = std::chrono::steady_clock::now();
//                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();
//                std::stringstream outLine;
//                outLine << "\033[A\33[2K\r";
//                outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
//                outLine << "#Posets in S[" << c-1 << "]: " << pNumber << "/" << S[c-1].size() << "\t";
//                outLine << "Time passed: " << duration << "ms";
//                std::cout << outLine.str() << std::endl; //remove std::endl on windows
//            }
/***********************************************************************************************************************************/
/**Deactivate Multithreading by commenting this block*/
            //thread start
            threads[idx] = std::thread(PosetObj::posetForward, std::ref(currentPoset), limit, std::ref(new_posets), std::ref(new_posets_mtx), std::ref(versionCounter));
            idx++;

            if(idx >= maxActiveThreads){
                posetProgress += maxActiveThreads;
                idx = 0;

                for(int i=0; i<maxActiveThreads; i++){
                    threads[i].join();

                    if(printProgress){
                        auto currentTime = std::chrono::steady_clock::now();
                        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();
                        std::stringstream outLine;
                        outLine << "\033[A\33[2K\r";
                        outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                        outLine << "Forwardsteps: " << i + posetProgress << "/" << S[c-1].size() << "\t";
                        outLine << "Time passed: " << duration << "ms";
                        std::cout << outLine.str() << std::endl; //remove std::endl on windows
                    }
                }
            }
        }
        for(int i=0; i<idx; i++){
            threads[i].join();

            if(printProgress){
                auto currentTime = std::chrono::steady_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime-startTime).count();
                std::stringstream outLine;
                outLine << "\033[A\33[2K\r";
                outLine << "\rn = " << n << ", c = " << c << "/" << cLeft << " ";
                outLine << "Forwardsteps: " << i + posetProgress << "/" << S[c-1].size() << "\t";
                outLine << "Time passed: " << duration << "ms";
                std::cout << outLine.str() << std::endl; //remove std::endl on windows
            }

/***********************************************************************************************************************************/
        }
        //copy to S
        for(auto it=new_posets.begin(); it!=new_posets.end(); it++){
            for(PosetObj poset : it->second){
                S[c].push_back(poset);
            }
        }
    }

    return S;
}


void PosetObj::posetForward(PosetObj& currentPoset, long long int limit, std::unordered_map<std::string, std::list<PosetObj>>& newPosets, std::shared_mutex& newPosetsMtx, std::unordered_map<std::string, int>& versionCounter){
    int n = currentPoset.Getn();
    int bound = 0;
    int currentNumSingletons = currentPoset.GetnumSingletons();
    if(currentNumSingletons > 2){
        bound = currentNumSingletons - 2;
    }
    bool singleton_comp = false; //to safe singleton comparisons

    for(int j=0; j < n-bound-1; j++){
        if(currentNumSingletons > 1 && j == n-bound-2){ //only true for last j
            singleton_comp = true;
        }
        for(int k=j+1; k<n-bound; k++){ //checks all combinations of ujuk until 2 singletons
            if(currentNumSingletons > 1 && k==n-bound-1 && !singleton_comp){ //last k
                continue; //skip comparisons with 2nd singleton
            }
            long long int p_1 = currentPoset.linExtTable[j][k];
            long long int p_2 = currentPoset.linExtTable[k][j];

            if(p_1 == 0 || p_2 == 0){ //comparing already related pair
                continue;
            }
            if(p_1 > limit || p_2 > limit){ //not sortable in remaining comparisons
                continue;
            } else {
                PosetObj new_poset_p1 = currentPoset; //copying
                PosetObj new_poset_p2 = currentPoset;

                if(p_1 >= p_2){ //add poset with greater number of linear extensions
                    new_poset_p1.add_edge(j, k);
                    new_poset_p1.transReduction(j, k);
                    new_poset_p1.SetlinExtensions(p_1);

                    new_poset_p2.add_edge(k, j);
                    new_poset_p2.transReduction(k, j);
                    new_poset_p2.SetlinExtensions(p_2);
                } else {
                    new_poset_p1.add_edge(k, j);
                    new_poset_p1.transReduction(k, j);
                    new_poset_p1.SetlinExtensions(p_2);

                    new_poset_p2.add_edge(j, k);
                    new_poset_p2.transReduction(j, k);
                    new_poset_p2.SetlinExtensions(p_1);
                }
            //isocheck
            isoCheckAndInsert(new_poset_p1, new_poset_p2, newPosets, newPosetsMtx, versionCounter);
            }
        }
    }
}

void PosetObj::isoCheckAndInsert(PosetObj& poset1, PosetObj& poset2, std::unordered_map<std::string, std::list<PosetObj>>& newPosets, std::shared_mutex& newPosetsMtx, std::unordered_map<std::string, int>& versionCounter){
    //calculate id1
    poset1.calculateAndSetIDs();
    //calculate id2
    poset2.calculateAndSetIDs();


    //locking read access
    newPosetsMtx.lock_shared();

    if(poset1.has_congruent_in(newPosets) || poset2.has_congruent_in(newPosets)){
        newPosetsMtx.unlock_shared();
        return; //found congruent poset
    } else{
        newPosetsMtx.unlock_shared();
        newPosetsMtx.lock();

        if(poset1.has_congruent_in(newPosets)){
            newPosetsMtx.unlock();
            return;
        }
        if(poset2.has_congruent_in(newPosets)){
            newPosetsMtx.unlock();
            return;
        }

        newPosets[poset1.GetidTag()].push_back(poset1);

        newPosetsMtx.unlock();
        return;
    }
}

/**experimental**/
std::vector<std::list<PosetObj>> PosetObj::forwardsteps2_MultiThread(int cLeft){
    std::vector<std::list<PosetObj>> S;
    std::mutex S_mtx;
    S.resize(cLeft + 1);
    if(cLeft > 0){
        S[0].push_back(*this);
    }

    auto startTime = std::chrono::steady_clock::now();

    for(int c=1; c < cLeft+1; c++){
        std::unordered_map<std::string, std::list<PosetObj>> new_posets;
        std::unordered_map<std::string, int> versionCounter;
        std::shared_mutex new_posets_mtx;
        long long int limit = 1LL << (cLeft-c);

        std::vector<std::thread> threads;
        int maxActiveThreads = std::thread::hardware_concurrency()*2;
        threads.resize(maxActiveThreads); //max 6*5 Threads
        int idx = 0;
        int posetProgress = 0;
        int pNumber = 0; //only Singlethread relevant

        for(PosetObj& currentPoset : S[c - 1]){
            //thread start
            threads[idx] = std::thread(PosetObj::parallelLinExtensions, std::ref(currentPoset));
            idx++;

            if(idx >= maxActiveThreads){
                posetProgress += maxActiveThreads;
                idx = 0;

                for(int i=0; i<maxActiveThreads; i++){
                    threads[i].join();
                }
            }
        }
        for(int i=0; i<idx; i++){
            threads[i].join();
        }

        idx = 0;
        posetProgress = 0;
        for(PosetObj& currentPoset : S[c - 1]){
            //thread start
            threads[idx] = std::thread(PosetObj::posetForward2, std::ref(currentPoset), limit, std::ref(new_posets), std::ref(new_posets_mtx), std::ref(versionCounter), cLeft, c);
            idx++;

            if(idx >= maxActiveThreads){
                posetProgress += maxActiveThreads;
                idx = 0;

                for(int i=0; i<maxActiveThreads; i++){
                    threads[i].join();
                }
            }
        }
        for(int i=0; i<idx; i++){
            threads[i].join();
        }
        //copy to S
        for(auto it=new_posets.begin(); it!=new_posets.end(); it++){
            for(PosetObj poset : it->second){
                S[c].push_back(poset);
            }
        }
    }

    return S;
}

void PosetObj::posetForward2(PosetObj& currentPoset, long long int limit, std::unordered_map<std::string, std::list<PosetObj>>& newPosets, std::shared_mutex& newPosetsMtx, std::unordered_map<std::string, int>& versionCounter, int cLeft, int c){
    int n = currentPoset.Getn();
    int bound = 0;
    int currentNumSingletons = currentPoset.GetnumSingletons();
    if(currentNumSingletons > 2){
        bound = currentNumSingletons - 2;
    }
    bool singleton_comp = false; //to safe singleton comparisons

    for(int j=0; j < n-bound-1; j++){
        if(currentNumSingletons > 1 && j == n-bound-2){ //only true for last j
            singleton_comp = true;
        }
        for(int k=j+1; k<n-bound; k++){ //checks all combinations of ujuk until 2 singletons
            if(currentNumSingletons > 1 && k==n-bound-1 && !singleton_comp){ //last k
                continue; //skip comparisons with 2nd singleton
            }
            long long int p_1 = currentPoset.linExtTable[j][k];
            long long int p_2 = currentPoset.linExtTable[k][j];

            if(p_1 == 0 || p_2 == 0){ //comparing already related pair
                continue;
            }
            if(p_1 > limit || p_2 > limit){ //not sortable in remaining comparisons
                continue;
            } else {
                PosetObj new_poset_p1 = currentPoset; //copying
                PosetObj new_poset_p2 = currentPoset;

                if(p_1 >= p_2){ //add poset with greater number of linear extensions
                    new_poset_p1.add_edge(j, k);
                    new_poset_p1.transReduction(j, k);
                    new_poset_p1.SetlinExtensions(p_1);
                    if (new_poset_p1.isDefinitelyNotSortable(cLeft-c)){
                        continue;
                    }

                    new_poset_p2.add_edge(k, j);
                    new_poset_p2.transReduction(k, j);
                    new_poset_p2.SetlinExtensions(p_2);
                    if (new_poset_p2.isDefinitelyNotSortable(cLeft-c)){
                        continue;
                    }
                } else {
                    new_poset_p1.add_edge(k, j);
                    new_poset_p1.transReduction(k, j);
                    new_poset_p1.SetlinExtensions(p_2);
                    if (new_poset_p1.isDefinitelyNotSortable(cLeft-c)){
                        continue;
                    }

                    new_poset_p2.add_edge(j, k);
                    new_poset_p2.transReduction(j, k);
                    new_poset_p2.SetlinExtensions(p_1);
                    if (new_poset_p2.isDefinitelyNotSortable(cLeft-c)){
                        continue;
                    }
                }
            //isocheck
            isoCheckAndInsert(new_poset_p1, new_poset_p2, newPosets, newPosetsMtx, versionCounter);
            }
        }
    }
}
