#ifndef POSETOBJ_H
#define POSETOBJ_H
#include <string>
#include <stdint.h>
#include <vector>
#include <mutex>
#include <shared_mutex>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>


class PosetObj {
    typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS> Graph;
public:
    PosetObj(Graph _graph);
    virtual ~PosetObj();
    PosetObj(const PosetObj& other);

    long long int GetlinExtensions() const{
        return linExtensions;
    }

    void SetlinExtensions(long long int val) {
        linExtensions = val;
    }

    int Getn() const{
        return n;
    }

    void Setn(int val) {
        n = val;
    }

    std::vector<std::vector<long long int>> GetlinExtTable() const{
        return linExtTable;
    }

    void SetlinExtTable(std::vector<std::vector<long long int>> val) {
        linExtTable = val;
    }

    std::pair<int, int> GetaddedEdge(){
        return addedEdge;
    }

    std::string GetidTag() const{
        return idTag;
    }

    void SetidTag(std::string val) {
        idTag = val;
    }

    std::string GetidRevTag() const{
        return idRevTag;
    }

    void SetidRevTag(std::string val) {
        idRevTag = val;
    }

    Graph Getgraph() const{
        return graph;
    }

    Graph& GetgraphRef(){
        return graph;
    }

    void Setgraph(Graph val) {
        graph = val;
    }

    Graph GetrevGraph() const{
        return revGraph;
    }

    void SetrevGraph(Graph val) {
        revGraph = val;
    }

    int GetnumSingletons() const{
        return singletons.size();
    }

    std::list<int> Getsingletons() const{
        return singletons;
    }

    void Setsingletons(std::list<int> val) {
        singletons = val;
    }

    int Getupperlimit() const{
        return upperlimit;
    }

    void Setupperlimit(int val) {
        upperlimit = val;
    }

    int Getlowerlimit() const{
        return lowerlimit;
    }

    void Setlowerlimit(int val) {
        lowerlimit = val;
    }

    static long long int factorial(int n){
        long long int res = 1;
        for(int i=2; i<n+1; i++){
            res *= i;
        }
        return res;
    }

    long long int calculateLinExtensions();
    void add_edge(int j, int k);
    void remove_edge(int j, int k);
    void print_poset();
    void printAddedEdge();
    std::vector<int> GetSuccessors(int node);
    std::vector<int> GetPredecessors(int node);
    std::vector<int> GetAdjacentVertices(int node);
    void calculateAndSetIDs();
    long long int calculateLinExtensionsSingleton();
    std::vector<std::list<PosetObj>> forwardsteps(int cLeft, bool printProgress=true);
    std::vector<std::list<PosetObj>> forwardsteps2(int cLeft, bool printProgress=false);
    std::vector<std::list<PosetObj>> backwardsteps(int cLeft, std::vector<std::list<PosetObj>>& S, bool printProgress=true);
    void transReduction(int outNode, int inNode);
    void transClosure();
    int numberOfUnrelatedPairs();
    bool is_isomorphic(PosetObj& other);
    bool is_rev_isomorphic(PosetObj& other);
    bool has_congruent_in(std::unordered_map<std::string, std::list<PosetObj>>& posetMap);
    bool has_congruent_in_list(std::list<PosetObj>& posetList, std::list<PosetObj>& posetRevList);
    std::pair<bool, PosetObj&> GetCongruentIn(std::unordered_map<std::string, std::list<PosetObj>>& posetMap);
    PosetObj& GetMatchFrom(std::unordered_map<std::string, std::list<PosetObj>>& posetMap);
    static PosetObj CreateLinOrder(int n);
    bool sortable(int cLeft);
    bool isDefinitelyNotSortable(int cLeft);
    bool isDefinitelySortable(int cLeft);
    bool has_two_sortable_succ(int cLeft);

    //Multithread methods
    std::vector<std::list<PosetObj>> forwardsteps_MultiThread(int cLeft, bool printProgress=true);
    std::vector<std::list<PosetObj>> forwardsteps2_MultiThread(int cLeft);
    std::vector<std::list<PosetObj>> backwardsteps_MultiThread(int cLeft, std::vector<std::list<PosetObj>>& S, bool printProgress=true);

    static void posetForwardCalculation(PosetObj& currentPoset, long long int limit,
                                 std::unordered_map<std::string, std::list<PosetObj>>& new_posets, std::unordered_map<std::string, std::mutex>& mtxmap,
                                 std::list<PosetObj>& Sc, std::mutex& S_mtx,
                                 int& pNumber, std::mutex& pNumberMtx);//
    void parallelInsert_ifNotCongruentIn(std::unordered_map<std::string, std::list<PosetObj>>& posetMap, std::unordered_map<std::string, std::mutex>& mtxmap, std::list<PosetObj>& Sc, std::mutex& S_mtx);//


    static void parallelLinExtensions(PosetObj& poset);
    static void posetForward(PosetObj& currentPoset, long long int limit, std::unordered_map<std::string, std::list<PosetObj>>& newPosets, std::shared_mutex& newPosetsMtx, std::unordered_map<std::string, int>& versionCounter);
    static void posetForward2(PosetObj& currentPoset, long long int limit, std::unordered_map<std::string, std::list<PosetObj>>& newPosets, std::shared_mutex& newPosetsMtx, std::unordered_map<std::string, int>& versionCounter, int cLeft, int c);
    static void isoCheckAndInsert(PosetObj& poset1, PosetObj& p2, std::unordered_map<std::string, std::list<PosetObj>>& newPosets, std::shared_mutex& newPosetsMtx, std::unordered_map<std::string, int>& versionCounter);

    //class variables
    static std::vector<std::unordered_map<std::string, std::list<PosetObj>>> S_starMap;
    static std::vector<std::unordered_map<std::string, std::list<PosetObj>>> S_compMap;
    static std::mutex atomLock;


protected:

private:
    long long int linExtensions;
    int n;
    std::vector<std::vector<long long int>> linExtTable;
    std::pair<int, int> addedEdge;
    std::string idTag;
    std::string idRevTag;
    Graph graph;
    Graph revGraph;
    std::list<int> singletons;
    int upperlimit;
    int lowerlimit;
};

#endif // POSETOBJ_H
